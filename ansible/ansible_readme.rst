#######################
Ansible - полезные вещи
#######################

::

   [ansible]$ ansible --version
   ansible [core 2.18.0]
     config file = /home/lucky/.ansible.cfg
     configured module search path = ['/home/lucky/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
     ansible python module location = /usr/lib/python3.12/site-packages/ansible
     ansible collection location = /home/lucky/.ansible/collections:/usr/share/ansible/collections
     executable location = /sbin/ansible
     python version = 3.12.7 (main, Oct  1 2024, 11:15:50) [GCC 14.2.1 20240910] (/usr/bin/python)
     jinja version = 3.1.4
     libyaml = True

   [ansible]$ cat /home/lucky/.ansible.cfg
   [defaults]
   host_key_checking = True
   inventory = /home/lucky/git-lucky-scripts/ansible/inventory.yaml
   # ansible_python_interpreter = /usr/bin/python3
   interpreter_python = auto_silent


*************
Adhoc команды
*************

$ ansible -b -m copy -a 'src=nginx.conf dest=/etc/nginx/nginx.conf owner=root group=root mode=644' vm2
$ ansible -b -m systemd_service -a 'name=nginx.service state=reloaded' vm2
$ ansible -m uri -a 'url="http://127.0.0.1/status/200"' vm2

$ ansible -b -m systemd_service -a 'name=nginx.service enabled=false state=stopped' vm2
$ ansible -b -m apt -a 'name=nginx state=absent' vm2
$ ansible -b -m file -a 'path=/etc/nginx state=absent' vm2



*************
Playbooks
*************

************************
Лаба из курса Devops 2.0
************************
