*****************************************
План презентации функциональности Ansible
*****************************************

Общие сведения
==============

Ansible works by connecting to your nodes and pushing out scripts called
“Ansible modules” to them. Most modules accept parameters that describe the
desired state of the system. Ansible then executes these modules (over SSH by
default), and removes them when finished. Your library of modules can reside on
any machine, and there are no servers, daemons, or databases required

Командная строка - рисунок
Модули
Переменные

Разграничение: **что** нужно сделать и **где** нужно сделать

Демонстрация: *setup*, *raw*, *copy*, *fetch*,  *file*, *debug*,

Демонстрация: *when*, *loop*

В каком-то смылсе ансибл - это СУ для, прежде всего, ОС, сетевый устройств.
ОМ терминал, U2000, Cisco Prime Assurance

Имена рекомендуется писать сразу на русском языке:
во-первых, потому как ничего не сломается (все современные Линукс ОС используют кодировку UTF-8);
во-вторых, ваши  файлы не будут использовать англоязычные пользователи;
в-третьих, понятнее.

Модули я рекомендую, но нигода не пишу, полные наименования а-ля FQDN для серверов

Вывод
=====

индексы в Python
ansible-doc и прочие ansible-*

автоматизация ИТ задач
yaml


Советы
======

Использование полных имён
Прямое указание инвентори

