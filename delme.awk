function join(array, start, end, sep,    result, i)
{
    if (sep == "")
        sep = " "
    else if (sep == SUBSEP) # magic value
        sep = ""
    result = array[start]
    for (i = start + 1; i <= end; i++)
        result = result sep array[i]
    return result
}

# awk  'BEGIN { FS=OFS="|" } {for (i=1; i<=NF; ++i) { $i=toupper(substr($i,1,1)) tolower(substr($i,2)); } print }'

BEGIN { FS=OFS="|" }
{ 
	s = $0
	sub(/^[^[:alpha:]]+/, "", s)
	sub(/[^[:alpha:]][[:digit:]]{5,}/, "", s)
	gsub(/[ _]+/, " ", s)
	sub(/[ _]\(www.lightaudio.+/, "", s)
	sub(/[ _]\[djdownloadme\.com.+/, "", s)
	sub(/[ _]\[EDM ARTHUR KING.+/, "", s)
	sub(/[ _]\[elektrobeats.org.+/, "", s)
	sub(/[ _]\(audio-paladin.ru.+/, "", s)
	sub(/[ _]MI4L.com.+/, "", s)
	sub(/\.mp3$/, "", s)
	$0 = s
	for (i = 1; i <= NF; ++i) {
		if ($i !~ /^and$/) $i = toupper(substr($i, 1, 1)) tolower(substr($i, 2))
		if ($i ~ /Feat\.\|Featuring\|FEAT\./) $i = "feat."
		$i = toupper(substr($i, 1, 1)) tolower(substr($i, 2))
		# printf("%s ", $i)
	}
print $0
}
