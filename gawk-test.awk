#!/usr/bin/gawk -f

# [lucky@larch git-lucky-scripts]$ cat f
# [2024-02-19 19:07:06.000] [stmp_12] [info] OKS_HDLC#21(ch=0)

BEGIN {
	FS="[[ :.-]"
	l = "[2024-02-19 19:07:06.000] [stmp_12] [info] OKS_HDLC#21(ch=0)"
}

1  # "1" без выражения {...} - это по дефолту /True/ {print $0}
END {
	print "c1:\t" NR
	print "c2:\t" systime() "\n\t" strptime("2024 12 16 20 19 37", "%Y %m %d %H %M %S")
	
	# Разбить строку l в лист a через разделитель
	# split(l, a, "[- :]")
	# print a[0] a[2] "-lucky-" a[1]
	
	print FS
	_ = " "  # space as the constant
	date1 = $2_$3_$4_$5_$6_$7
	print "c3:\t" date1
	print "c4:\t" systime() "\n\t" strptime(date1, "%Y %m %d %H %M %S")
	date2 = substr(l, 2, 4) _ substr(l, 7, 2) _ substr(l, 10, 2) _ substr(l, 13, 2) _ substr(l, 16, 2) _ substr(l, 19, 2)
	print "c5:\t" date2
	print "c6:\t" l
	print date2 == date1
}

# :[lucky@larch git-lucky-scripts]$ ./gawk-test.awk --load time f
# c1:	3
# c2:	1708105121
# 	1734369577

# :[lucky@larch git-lucky-scripts]$ ./gawk-test.awk  --load time f
# [2024-02-19 19:07:06.000] [stmp_12] [info] OKS_HDLC#21(ch=0)
# c1:	1
# c2:	1708436158
# 	1734369577
# [[ :.-]
# c3:	2024 02 19 19 07 06
# c4:	1708436158
# 	1708358826
# c5:	2024 02 19 19 07 06
# c6:	[2024-02-19 19:07:06.000] [stmp_12] [info] OKS_HDLC#21(ch=0)
# 1

