#!/usr/bin/sed

# Dosem — Beach Kisses (2023 Extended Mix) (www.lightaudio.ru).mp3

s/\s\+/ /g
s/—/-/g
s/\s*(www.lightaudio.ru)\s*//g
