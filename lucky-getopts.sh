#! /usr/bin/bash

PT="Progressive Trance"

# getopts "g:p:" tcon

# g=$OPTARG
#
#
# p=${OPTARG[1]}
# echo g=$g
# echo tcon=$tcon
# echo p=$p
echo -------------------------------------
echo `date "+%T"`
echo -------------------------------------

usage() {
	echo -e "Usage:\n$0 [-l | -g <B|DH|DnB|H|HTe|HT|P|PH|PT|Pop|T|TH|Te|UT>] TARGET\n" 1>&2
	echo $0 -h 1>&2
	echo -e "\nExamples:" 1>&2
	echo -e "$0 -l ~/mz7/delme" 1>&2
	echo -e "$0 -g PT ~/mz7/delme" 1>&2
	exit 1
}

while getopts "hlg:" o; do
	echo optind = $OPTIND
	case "${o}" in
		h)
			usage
			;;
		g)
			g=${OPTARG}
			[ $g = "PT" ] || [ $g = "DnB" ] || usage
			tcon=$(eval echo \$$g)
			echo g = $g
			echo tcon = $tcon
			#((g == 45 || g == 90)) || usage
			;;
		l)
			DIR="$(realpath $2)"
			echo DIR: $DIR
			echo =========
			echo File list
			echo =========
			IFS=$'\n'; for f in `find "$DIR" -maxdepth 1 -iname "*.mp3" -printf "%f\n"`; do
				echo "${f}"
				# echo
			done
			;;

#		\?)
#			echo \? option matched; exit 1

# usage
# ;;
    esac
done

shift $((OPTIND-1))


#if [ -z "${s}" ] || [ -z "${p}" ]; then
#    usage
#fi

echo
echo time: $DURATION

# Доделать: если опция -l не выбрана присвоить значение постоянной DIR
if [ -z $DIR ]; then
	echo DIR is Null
else
	echo DIR: $DIR
fi

