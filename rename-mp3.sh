#! /usr/bin/bash
# vim: sts=4:sw=4:expandtab

: << 'SKIP'
[lucky@larch mz7]$ id3v2 -l pod-v-12082360m22sdave_clarke_whitenoise_919_hq.mp3
id3v2 tag info for pod-v-12082360m22sdave_clarke_whitenoise_919_hq.mp3:
TPE1 (Lead performer(s)/Soloist(s)): Dave Clarke's Whitenoise
TIT2 (Title/songname/content description): Show 919
TALB (Album/Movie/Show title): www.daveclarke.com
TRCK (Track number/Position in set): *Invasive Machinery - David Carretta *So So Strong - Decius *Spirits are Here (The Dark Robot Remix) - Artiom *Follow Me - Third Culture*Paranoid Visions - Delectro *Come Around - Terence Fixmer *Calis 'N' New Yorkers (Disruptive Pattern Material Remix) - Fear E *Illustris - Jeroen Search*Albedo - George Tounisidis*Free Mind - DJ Dextro*Rhythmic Trip - LEGA & Cvbes *Hi-Tech Primitive - Jeran Portis *Tracking Failure - PWCCA*Klokklik - Steve Larson *Traditions - Spaaax


TYER (Year): 2023
TCON (Content type): Techno (18)
COMM (Comments): ()[]: podcast originated by RT�Radio - www.rte.ie/radio - please report issues to mrspring@mrspring.net
TXXX (User defined text information): (CodingHistory):
TXXX (User defined text information): (OrigDate):
TXXX (User defined text information): (Originator):
TXXX (User defined text information): (OrigTime):
TXXX (User defined text information): (TimeReference):
pod-v-12082360m22sdave_clarke_whitenoise_919_hq.mp3: No ID3v1 tag


Dosem — Beach Kisses (2023 Extended Mix) (www.lightaudio.ru).mp3
SKIP

# Константы

# B="Breaks"
# DH="Deep House"
# DnB="DnB"
# H="House"
# HTe="Hard Techno"
# HT="Hard Trance"
# P="Progressive"
# PH="Progressive House"
# PT="Progressive Trance"
# Pop="Pop"
# T="Trance"
# TH="Tech House"
# Te="Techno"
# UT="Uplifting Trance"
TYER="2024"

# В качестве директории последний параметр в команде
# todo
# DIR=${@: -1}
DIR=.
# Default value for files
: ${FILES:=$(find "$DIR" -maxdepth 1 -iname "*.mp3" -printf "%f\n")}
: ${f:="*.mp3"}
PATTERN='[ _][-—][ _]'


# Функции
usage() {
echo "Usage:
$0 [-hlr] [-f *.mp3] [-g <genre>] [<directory>]

Examples:
$0 -h
$0 -l ~/mz7/delme
$0 -g PT ~/mz7/delme
$0 -g "" ~/mz7/delme
" 1>&2
exit 1
}

# Обработка опций
while getopts "hlrf:g:" o; do
    echo optind: $OPTIND
    case "${o}" in
        f)
            f=${OPTARG}
            # Ignore case for ls
            shopt -s nocaseglob
            FILES="$f"
            shopt -u nocaseglob
            ;;
        h)
            usage
            ;;
        g)
            g=${OPTARG}
            # [ $g = "PT" ] || [ $g = "DnB" ] || usage
            if [ $g != '""' ]; then
                tcon=$(eval echo \$$g)
            fi
            echo g: $g
            #((g == 45 || g == 90)) || usage
            ;;
        l)
            echo files: "$f"
            # ".mp3" - default value for $f
            # : ${f:=".mp3"}
            echo Dir: $DIR
            echo
            echo =========
            echo File list
            echo =========
            echo
            echo "${FILES}" | nl
            echo
            exit 0
            ;;
        r)
            run=yes
            ;;
#       \?)
#           echo \? option matched; exit 1

# usage
# ;;
    esac
done

# shift $((OPTIND-1))

# main
echo "############ Main ############"
# echo -e \\nfl:\\n"$FILES"
cd "$DIR"; pwd

IFS=$'\n'; for i in $FILES; do
    # Разделитель в виде паттерна
    if [[ "$i" =~ $PATTERN ]]; then
        tpe1=$(echo "$i" | gawk -v FS="$PATTERN" -v IGNORECASE=1 '{
            s = $1
            sub(/^[^[:alpha:]]+/, "", s)
            sub(/ и /, " \\& ", s)
            gsub(/[ _]+/, " ", s)
            # Следующая секция предназначена для исправления регистра символов
            # - должен применяться т. н. Title Case. Через gensub backreference
            # сделать это нельзя, т. к. возвращается строка целиком, а
            # конструкция `arr[i] = gensub(arr[i], /[[:alpha:]]/,
            # toupper("\\1")` не работает - `toupper("\\1") = "\\1"`
            split(s, arr, " ")
            for (i in arr) {
                arr[i] = tolower(arr[i])
                position = match(arr[i], /[[:alpha:]]/)

                # Если pos = 0, пропустить составление слова. Был обнаружен баг
                # с задваиванием символов - связано со следующей работой awk:
                # $ echo "12345" | gawk "{$0 = substr($0, 0, 1); print}"
                # 1
                # $ echo "12345" | gawk "{$0 = substr($0, 1, 1); print}"
                # 1
                # для позиции 0 и 1 всегда выводится первый символ, кавычки
                # заменены с одинарных на двойные, что не ломать внешний бэш
                if (position != 0)
                    arr[i] = substr(arr[i], 1, position - 1)\
                          toupper(substr(arr[i], position, 1))\
                          substr(arr[i], position + 1)
                print arr[i]
            }
        }')
        # Можно сделать и так, но разделитель FS уже описан, и его не поменять
        # for (i = 1; i <= NF; ++i) {
        #         if ($i !~ /^and$/) $i = toupper(substr($i, 1, 1)) tolower(substr($i, 2))
        #         if ($i ~ /Feat\.\|Featuring\|FEAT\./) $i = "feat."
        #         $i = toupper(substr($i, 1, 1)) tolower(substr($i, 2))
        # }
        # print $0
        tit2=$(echo "$i" | gawk -v FS="$PATTERN" -v IGNORECASE=1 '{
            s = $2
            sub(/^[^[:alpha:]]+/, "", s)
            sub(/[^[:alpha:]][[:digit:]]{5,}/, "", s)
            gsub(/[ _]+/, " ", s)
            sub(/[ _]\(www.lightaudio.+/, "", s)
            sub(/[ _]\[djdownloadme\.com.+/, "", s)
            sub(/[ _]\[EDM ARTHUR KING.+/, "", s)
            sub(/[ _]\[elektrobeats.org.+/, "", s)
            sub(/[ _]\(audio-paladin.ru.+/, "", s)
            sub(/[ _]MI4L.com.+/, "", s)
            sub(/\.mp3$/, "", s)
            split(s, arr, " ")
            for (i in arr) {
                arr[i] = tolower(arr[i])
                position = match(arr[i], /[[:alpha:]]/)
                if (position != 0)
                    arr[i] = substr(arr[i], 1, position - 1)\
                          toupper(substr(arr[i], position, 1))\
                          substr(arr[i], position + 1)
                print arr[i]
            }
            # print s  # Применялся, когда не было исправления регистра
        }')
        # awk скрипт формирует имена в нескольких строках. Чтобы их легко объединить, используется хитрость - echo без кавычек
        tit2=$(echo $tit2)
        tpe1=$(echo $tpe1)

    # Нераспарсенные имена файлов
    else tpe1=---NULL; tit2=---NULL
    fi

    # Printing the table to console
    echo "$tpe1" | gawk '{
        s=substr($0, 0, 35)
        printf "%35-s\u2502", s
    }'
    echo "$tit2" | gawk '{
        s=substr($0, 0, 35)
        printf "%35-s\u2502", s
    }'
    echo "$i"
    
    # Если имя файла содержит разделитель $PATTERN и есть флаг -r, то проставляются тэги id3v2
    if [[ "$i" =~ $PATTERN ]] && [ -n "$run" ]; then
        # id3v2 -a "$tpe1" -t "$tit2" -g "$tcon" -A "" -c "" --TPE2 "" -r TYER -r COMM "$i"
        id3v2 -a "$tpe1" -t "$tit2" -g "$tcon" -A "" -c "" --TPE2 "" "$i"
        # Strip id3v1 tags
        echo -n "---------------- "
        id3v2 -r COMM "$i"
        echo -n "---------------- "
        id3v2 -s "$i"
        # Переименование производится через foobar2000 на целевом lucky-n56
        # mv -v "$i" "$tpe1 — $tit2.mp3"
    fi
    if [[ ! "$i" =~ $PATTERN ]]; then
        echo "[ERROR] No delimiter for file '$i'"
    fi
    echo
done
