#!/usr/bin/bash
# vim: ts=4:sts=4:sw=4:expandtab

IN1="
.bashrc
.config/alacritty/alacritty.toml
.config/kitty/kitty.conf
.config/lf/lfcd.sh
.config/lf/lfrc
.config/lxc/default.conf
.config/mimeapps.list
.config/picom/picom.conf
.config/qtile/autostart.sh
.config/qtile/config.py
.config/zathura/zathurarc
.vimrc
"
OUT1="$HOME/git-lucky-configs"
# Для отладки
# OUT1="/tmp/sync"
for f in $IN1; do
    # Крайний / мешал синхронизации конфигов из $HOME
    # rsync --mkpath "$HOME/$f" "$OUT1/${f%/*}/"
    # rsync -n -P "$HOME/$f" "$OUT1/${f%/*}"
    rsync "$HOME/$f" "$OUT1/${f%/*}"
    if [ $? == 0 ]; then
        echo "------------ Synced: $f ✅"
    else 
        echo "============ Что-то пошло не так: $f ❌"
    fi
done
