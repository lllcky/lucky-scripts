#!/usr/bin/gawk -f

BEGIN {FS = "[:,] "}
/^202/ {
	s = $2
	# Замена конкретных id на xxxxx для подсчета количества типовых ошибок
	sub(/-[[:digit:]]+-[[:digit:]]+,[[:digit:]]+,main\]/, "-x-x,x,main]", s)
	sub(/RequestId/, "RequestId: xxxxxx", s)
	sub(/[[:digit:]]{8}/, "xxxxxxxx", s)
	sub(/block id=[[:digit:]]+/, "block id=xxxx", s)
	sub(/with id [[:digit:]]+ timeout/, "with id xxxx timeout", s)
	sub(/Session for user [[:digit:]]+ already/, "Session for user xx already", s)
	sub(/@.+/, "", s)
	sub(/[[:space:]\.]+$/, "", s)
	sub(/housekeeper delta=.+)/, "housekeeper delta=XXsXXXms)", s)
	if ($0 ~ / ERROR /)
		print "ERROR " s
	if ($0 ~ / WARN /)
		print "WARN " s
}

/^Caused by:/ {print "JAVA " $0}
