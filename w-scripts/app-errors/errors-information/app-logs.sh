#!/bin/bash

# скрипт по всем ошибкам АРР за день
# параметры
log_file=/tmp/logs-app-$(date +'%Y%m%d').txt
day1=$(date -d -1day +'%Y-%m-%d')
# adresat="vvtaraba@mts.ru kayersh2@mts.ru aazveko1@mts.ru aakuvay1@mts.ru dylendel@mts.ru yachich3@mts.ru"
# adresat="vvtaraba@mts.ru"
adresat="STS_MSO_Group_TAP@mts.ru"
adresat="asakulin1@mts.ru"
prog_file="/opt/sorm/scripts/app-errors-types.awk"
prog_file_2="/opt/sorm/scripts/app-parse.awk"
err_file="/opt/sorm/scripts/app-errors-types.txt"
err_file_2="/opt/sorm/scripts/app-errors.txt"
html_file="/opt/sorm/scripts/app-errors-parsed.html"


# Функция проверяет в логе JAVA еxceptions - в случае наличия добавляется аргумент в mailx
optional_html() {
    if test "$(grep JAVA\  $err_file2)"; then
        echo -n "-a \$html_file"
    fi
}


# IFS=$'\n'
tempfile=$(mktemp "${TMPDIR:-/tmp}/$(basename $0)_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

# ------------------------------------------------------------------------------
# за вчера ошибки ERROR и WARN в логах APP
# лист лог
find /opt/sorm-adapter/logs/${day1} -type f -name 'application*log.gz' |sort -V > $tempfile

printf "\\n **************************************\\n" >> ${log_file}
printf "\\n ****** LOGS APP ${day1} ********\\n"       >> ${log_file}
printf "\\n **************************************\\n" >> ${log_file}

for logfile in $(cat $tempfile); do
	if [ -f "$logfile" ];
		then {
			printf "\\n ------------------------------------\\n"			>> ${log_file}
			printf "\\n -- ${day1} $(echo $logfile |cut -d '/' -f 6-) --\\n"	>> ${log_file}
			printf "\\n -- ERROR ----------------------------\\n"			>> ${log_file}
			zcat $logfile | grep " ERROR "						>> ${log_file}
			printf "\\n ------------------------------------\\n"			>> ${log_file}
			printf "\\n -- WARN -----------------------------\\n"			>> ${log_file}
			zcat $logfile | grep " WARN "						>> ${log_file}
		}
	fi
done

rm -f "$tempfile"

# Парсинг лог-файла и вывод разбора в $err_file для тела письма и err_file_2 для вложения
gawk -f $prog_file ${log_file} | tee $err_file_2 | sort | uniq -c > $err_file
# Добавление пустой строки (\n) в начало файла - отладка
sed -i '1i\\' $err_file_2
cat > $html_file << EOF
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style>
table {
  font-family: sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size: 10pt;
  margin-block-end: 16pt;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 6px;
}

p {
  font-family: sans-serif;
  /*border-collapse: collapse;*/
  width: 100%;
  font-size: 11pt;
}

caption {
    text-align: left;
    padding: 8px;
    font-size: 11pt;
}
<!-
tr:nth-child(even) {
  background-color: #dddddd;
}
->
</style>
</head>
<body>
    <table>
$(gawk -f $prog_file_2 $err_file_2)
    </table>
</body>
</html>
EOF


# ------------------------------------------------------------------------------
# отчет
# защита если нет еррор файла и если лог файл > 1MB (1048576 B or 2MB 2097152)
if [[ $(wc -c ${log_file} | awk '{ print$1 }') -le 2097152 ]]; then {
printf "$(hostname)
Все ошибки ERROR и WARN по логам app (application*log.gz) за вчера ${day1}.

Количество ошибок и предупреждений в приложенном логе:

$(cat $err_file)

Количество разных типов ошибок: $(cat $err_file | wc -l)
" | mailx -s "logs APP на $(date +'%Y%m%d %H:%M')" optional_html $adresat
}
else {
tar czf ${log_file}.tgz ${log_file}
sleep 3
printf "$(hostname)
Все ошибки ERROR и WARN по логам app (application*log.gz) за вчера ${day1}.
Лог большой, поэтому в архиве.

Количество ошибок и предупреждений в приложенном логе:

$(cat $err_file)

Количество разных типов ошибок: $(cat $err_file | wc -l)
" | mailx -s "logs APP на $(date +'%Y%m%d %H:%M')" -a ${log_file}.tgz optional_html $adresat
}
fi

# забекапить логи, не делаем с конца 2019, тк ELK
# tar -czf /opt/felix-sorm/logs/$(date -d '-1 day' +'%F').tgz /opt/felix-sorm/logs/$(date -d '-1 day' +'%F')
# mv /opt/felix-sorm/logs/$(date -d '-1 day' +'%F').tgz /opt/felix-sorm/logs/backups-log-app-for-razbor/
#
# cd /opt/felix-sorm/logs/
# tar -czf $(date -d '-1 day' +'%F').tgz $(date -d '-1 day' +'%F')
# mv $(date -d '-1 day' +'%F').tgz backups-log-app-for-razbor/
# ll backups-log-app-for-razbor/
# tar -tf backups-log-app-for-razbor/2017-12-12.tgz

exit 0
