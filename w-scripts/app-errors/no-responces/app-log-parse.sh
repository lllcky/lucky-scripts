#!/usr/bin/bash
# vim:ts=4:sts=4:sw=4:et

# LOG - "сырой" лог, OUT - обработанный лог
# Если агрумент при выполнении скрипта не указывается, используется значение
# активного лога  - '/opt/sorm-adapter/logs/application.log'
LOG="$1"
: ${LOG:="/opt/sorm-adapter/logs/application.log"}
OUT="$(pwd)/app-requests.log"
PROGRAM="create-task-request.awk"
# PROGRAM="cert-expired-remove.awk"
TEMP1=$(mktemp)
TEMP2=$(mktemp)
RE_DATE='202[[:digit:]](-[[:digit:]]{2}){2}'
RE_TIME='([[:digit:]]{2}[:\.]){3}[[:digit:]]{3}'

# Обработка "сырого" лога: sed добавляет пустую строку перед датами для
# разделения записей (в логах они не всегда отделены друг от друга пустой
# строкой); uniq удаляет дублирующиеся (подряд) строки; скрипт PROGRAM
# формирует записи, где разделители полей - перенос строки, разделители записей
# - пустая строка, т. е. два переноса строки
sed '/^202/i\\' $LOG > $TEMP1
uniq $TEMP1 $TEMP2
gawk '
    BEGIN {
        RS = ORS = "\n\n"
        FS = OFS = "\n"
        IGNORECASE = 1
    }
    # Force record to be reconstituted [gawk.pdf, page 73]
    {$1 = $1}
    /data CHOICE : create-task-request/
    /data CHOICE : create-task-response/
' $TEMP2 > $OUT

# Вывод информации в stdout
# echo -e "▶ Количество строк:\n$(wc -l $LOG $TEMP1 $TEMP2 $OUT)\n"
echo -e "▶ Исходный лог: \033[34m$LOG\033[0m\n"
echo -e "▶ Обработанный лог: \033[36m$OUT\033[0m"
gawk -v RS="\n\n" -v FS="\n" 'END {print "Всего записей:", NR}' $OUT
# Debug: вывод записей запросов, где есть 7 "?" подряд, выделение красным
# цветом. Меняя паттерн // и дублируя строку ниже можно дополнять вывод
# запросов с проблемами
echo -e "\033[31m$(gawk -v RS="\n\n" -v FS="\n" '/?{7}/ {print "7 \"?\" подряд в записях №" NR "\n"}' $OUT)\033[0m\n"

# Скрипт message-id.awk формирует номера **записей** (NR - Number of Record)
# запросов, которые остались без ответа. Последующий grep выводит номера
# *первых* строк (с timestamp) в обработанном и исходном файлах. Пример в конце
# файла
no_responces=$(gawk -f message-id.awk $OUT)
echo -e "▶ Список записей (NR) без ответа:"
# echo "$no_responces" | tee no_responces.txt | nl
echo "$no_responces" | nl

echo -e "\n▶ Запросы без ответа - номера строк в обработанном файле $OUT:"
# Для каждого NR из *no-responses* вывести первую строку, а потом grep для
# отображения её номера в обработанном и исходном логах
truncate -s0 $TEMP1 $TEMP2
for line in $no_responces; do
    first_string="$(gawk -F "\n" -v RS="" -v l=$line 'NR == l {print $1}' $OUT)"
    grep -Fn -e "$first_string" $OUT >> $TEMP1
done
cat -n $TEMP1

echo -e "\n▶ Запросы без ответа - номера строк в исходном файле $LOG:"
for line in $no_responces; do
    first_string="$(gawk -F "\n" -v RS="" -v l=$line 'NR == l {print $1}' $OUT)"
    grep -Fn -e "$first_string" $LOG >> $TEMP2
done
cat -n $TEMP2
rm $TEMP1 $TEMP2

# Опциональная запись запросов без ответа, если при вызове скрипта есть второй аргумент -p
if [  "$2" = "-p" ]; then
    echo -e "\n▶ Запросы без ответа записаны по отдельным файлам:"
    for record in $no_responces; do
        gawk -F "\n" -v RS="" -v record=$record '
            NR == record {
                # Вытаскивание из первой строки даты и времени в *arr* и
                # формирование имени файла: "2025-01-15 17:44:43.762" →
                # "req-2025-01-15-174443.762-nr597.txt"
                match($1, /(202[[:digit:]](-[[:digit:]]{2}){2}) (([[:digit:]]{2}[:\.]){3}[[:digit:]]{3})/, arr)
                date_time = arr[1]"-"arr[3]
                gsub(/:/, "", date_time)
                filename = "req-" date_time "-nr" NR ".txt"
                print filename >> "requests.txt"
                print $0 > filename
            }
            ' $OUT
    done
    cat -n "requests.txt" && rm "requests.txt"
    echo
fi


: << Пример_вывода
[app-errors]$ ./app-log-parse.sh $f -p
▶ Количество строк:
   733136 /mnt/c/w/avaria-20250116/application.15012025.log
   940966 /tmp/tmp.ZhnxeUiMII
   893838 /tmp/tmp.azUlxAQCY2
   105492 /mnt/c/w/git-lucky-scripts/w-scripts/app-errors/app-requests.log
  2673432 total

▶ Исходный лог: /mnt/c/w/avaria-20250116/application.15012025.log

▶ Обработанный лог: /mnt/c/w/git-lucky-scripts/w-scripts/app-errors/app-requests.log

7 "?" подряд в записи №828

▶ Список записей (NR) без ответа:
     1  597
     2  828
     3  1763

▶ Запросы без ответа - номера строк в обработанном файле /mnt/c/w/git-lucky-scripts/w-scripts/app-errors/app-requests.log:
     1  15859:2025-01-15 17:44:43.762 DEBUG 14895 --- [yar_m5-10-0] r.m.s.c.s.context.ControlChannelContext  : Read message id 4282, data value Message ::= {
     2  59946:2025-01-15 17:55:59.418 DEBUG 14895 --- [yar_m5-10-8] r.m.s.c.s.context.ControlChannelContext  : Read message id 5146, data value Message ::= {
     3  85494:2025-01-15 19:17:45.300 DEBUG 14895 --- [yar_m5-28-6] r.m.s.c.s.context.ControlChannelContext  : Read message id 47, data value Message ::= {

▶ Запросы без ответа - номера строк в исходном файле /mnt/c/w/avaria-20250116/application.15012025.log:
     1  137121:2025-01-15 17:44:43.762 DEBUG 14895 --- [yar_m5-10-0] r.m.s.c.s.context.ControlChannelContext  : Read message id 4282, data value Message ::= {
     2  270061:2025-01-15 17:55:59.418 DEBUG 14895 --- [yar_m5-10-8] r.m.s.c.s.context.ControlChannelContext  : Read message id 5146, data value Message ::= {
     3  448870:2025-01-15 19:17:45.300 DEBUG 14895 --- [yar_m5-28-6] r.m.s.c.s.context.ControlChannelContext  : Read message id 47, data value Message ::= {

▶ Записаны запросы по отдельным файлам:
     1  req-2025-01-15-174443.762-nr597.txt
     2  req-2025-01-15-175559.418-nr828.txt
     3  req-2025-01-15-191745.300-nr1763.txt
Пример_вывода
