# vim:ts=4:sts=4:sw=4:et

BEGIN {
    RS = ORS = "\n\n"
    FS = OFS = "\n"
    IGNORECASE = 1
}
# Force record to be reconstituted [gawk.pdf, page 73]
{$1 = $1}
/certificate/ {next}
{print $0}

# Пример использования скрипта
# ============================
#
# Исходный файл f.txt
# $ cat f.txt
# 1
# 2
#
# 3
#
# 4
#
# 5.1
# 5.2certificate
# 5.3
#
# 6
# 7
#
# 8 certificate_expired
#
# 9
# 10
# 11
# 12
#
# 13 Certificate
# 14
# 15
# $ gawk -f cert_expired_remove.awk f.txt > fout.txt
# $ diff -u10 f.txt fout.txt
# --- f.txt       2025-01-16 17:30:56.462013800 +0300
# +++ fout.txt    2025-01-16 17:35:08.806166500 +0300
# @@ -1,24 +1,15 @@
#  1
#  2
#
#  3
#
#  4
#
# -5.1
# -5.2certificate
# -5.3
# -
#  6
#  7
#
# -8 certificate_expired
# -
#  9
#  10
#  11
#  12
#
# -13 Certificate
# -14
# -15
