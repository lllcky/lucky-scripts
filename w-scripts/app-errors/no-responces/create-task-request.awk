# vim:ts=4:sts=4:sw=4:et

BEGIN {
    RS = ORS = "\n\n"
    FS = OFS = "\n"
    IGNORECASE = 1
}
# Force record to be reconstituted [gawk.pdf, page 73]
{$1 = $1}
/data CHOICE : create-task-request/
/data CHOICE : create-task-response/
