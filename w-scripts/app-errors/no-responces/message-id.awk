# vim:ts=4:sts=4:sw=4:et

# Принцип работы скрипта. Разделитель записей - пустая строка, разделитель полей - перенос строки (\n). Cкрипт выполняется на обработанном логе, где все записи разделены пустой строкой. Запросы в формате "process message-id" пишутся в массив *r*, ответы в том же формате - в массив *R*. Далее для каждого R[i] проверяется наличие в r[j]. В случае совпадения значений, элемент R[i] удаляется из *r*. Таким образом, в *r* (запросы) остаются элементы, для которых нет соответствующих ответов. Номера записей (NR) записываются в файл no-resonse.txt
BEGIN {
    RS = "\n\n"
    FS = "\n"
    IGNORECASE = 1
}
/create-task-request/ {
    # Эта функция копирует в массив *arr* идентификаторы process-id, pu-id и message-id из **первой** строки записи, то же делается и для response
    match($1, /DEBUG ([[:digit:]]+).+\[([[:alnum:]_-]+)\].+id:? +([[:digit:]]+)/, arr)
    r[NR] = arr[1]" "arr[2]" "arr[3]
    # print "r:", NR, r[NR]
}
/create-task-response/ {
    match($1, /DEBUG ([[:digit:]]+).+\[([[:alnum:]_-]+)\].+id:? +([[:digit:]]+)/, arr)
    R[NR] = arr[1]" "arr[2]" "arr[3]
    # print "R:", NR, R[NR]
}
END {
    # print length(r), length(R)
    for (response in R)
        for (request in r)
            if (R[response] == r[request])
                delete r[request]
    for (request in r)
        print request
}
