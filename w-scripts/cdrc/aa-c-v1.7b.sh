#!/usr/bin/env bash

### Constants ###
MINS=1440
DIRSFILE="/home/$USER/aa/dirs60.txt"
FQDN="$(hostname -f)"
HOSTNAME="$(hostname)"
IP="$(/usr/sbin/ip -br a | gawk -F"[ /]+" '/10\.147/{print $3}')"
USER=$(whoami)
CSVFINAL="/home/$USER/aa/caches-final.csv"
CSVTEMP="/home/$USER/aa/caches-temp.csv"
FILELIST="/home/$USER/aa/caches-filelist.csv"
HTMLFILE="/home/$USER/aa/notification.html"
TODAY=$(date +%Y/%m/%d)
YESTERDAY=$(date +%Y/%m/%d -d yesterday)

#set -x

### Functions ###
# Подсчёт разницы в секундах между текущим временем и временем модификации файла
datediff() {
    local tnow=$(date +%s);
    local tdelta=$(( $tnow - $(date -d "$1" +%s) ))
    # Вычисление количества дней (часов) между датами до целого значения с округлением вниз (bash не может работать с float point)
    echo "$(( $tdelta / 3600 ))h"
    return
}

# Функция "вырезает" поле csv файла (аргумент $1) из файла (аргумент $2)
csv_field() {
    echo $2 | gawk -F; "{ print \$($1) }"
    return
}

# Функция подкрашивает строку в html таблице, если директория не обновлялась более 1 суток (статус 1 в csv файле)
red1() {
    if [ $(csv_field 4 $line) -ne 0 ] || [ $(csv_field 6 $line) -ne 0 ]; then
        echo -n ' style="color:brown;background:blanchedalmond"';
    fi
    return
}

# Функция подкрашивает строку в html таблице, если статус proftpd - inactive
red2() {
    if [ "$(echo $1 | grep -o inactive)" == inactive ]; then
        echo -n ' style="color:brown;background:blanchedalmond"';
    fi
    return
}

### Main program ###
# Формирование списка директорий до уроня потока
dirs=$(find /data/MRG[^T]* -maxdepth 1 -mindepth 1 -type d | sort)
echo "$dirs" > $DIRSFILE

# Формирование CSV файлов: статистика и листинг просроченных файлов за $MINS
find /data/MRG[^T]*/*/caches -maxdepth 0 -type d -mtime 0 -printf "server$1;%p;%TF %TH:%TM;0\n" > $CSVTEMP
find /data/MRG[^T]*/*/caches -maxdepth 0 -type d -mmin +$MINS -printf "server$1;%p;%TF %TH:%TM;1\n" >> $CSVTEMP
find /data/MRG[^T]*/*/caches -type f -mmin +$MINS -printf "server$1;%p;%TF %TH:%TM;%s\n" > $FILELIST
sort $CSVTEMP -o $CSVTEMP
sort $FILELIST -o $FILELIST

# Технический вывод
echo "\$CSVTEMP $(wc -l $CSVTEMP)"
echo "\$FILELIST $(wc -l $FILELIST)"

# Удаление старого csv файла
rm -f $CSVFINAL

# Добавление полей 4-7 в csv файл (построчно)
IFS=$'\n'; for line in $(cat $CSVTEMP); do
    # Количество "зависнувших" файлов в caches, модифицированных более $MINS суток назад
    caches_cdr=$(find $(csv_field 2 $line) -type f -mmin +$MINS | wc -l)

    # Количество постоянных файлов cdr во всех вложенных директориях, кроме caches
    cdr_total=$(find $(echo $line | gawk -F; '{print $2}' | sed 's/caches/[^c]*/') -type f | wc -l)

    cdr_total_fmt=$(echo $cdr_total | numfmt --to=si)

    # l - это line с вырезанным /data/MRGXX/YYYY
    l=$(echo $line | awk 'BEGIN {FS="[/;]"; OFS="/"} {print ""; $3; $4; $5}')
    # то же можно сделать через sed
    # l=$(echo $line | sed 's/.\+data/data/; s/\/caches.*//')

    # Подсчёт CDR за последние сутки. Для ускорения смотрятся директории только за сегодня и вчера
    cdr_t=$(find $(echo $l/$TODAY) -type f -not -mmin +$MINS | wc -l)
    cdr_y=$(find $(echo $l/$YESTERDAY) -type f -not -mmin +$MINS | wc -l)
    cdr24=$((cdr_t + cdr_y))
    cdr24_fmt=$(echo $cdr24 | numfmt --to=si)

    # Конкатенация полей csv файла
    #     f1-4--f5-------------------------------f6----------f7---------f8-------------f9--------
    echo "$line;$(datediff $(csv_field 3 $line));$caches_cdr;$cdr_total;$cdr_total_fmt;$cdr24_fmt" >> $CSVFINAL
done

proftpd_status="$(systemctl status proftpd | grep Active: | sed 's/.\+Active: //')"
rm -f $CSVTEMP

# Формирование html файла
cat > $HTMLFILE << EOS
<h3 style="background-color:LightGray; font-family:sans-serif;">Сервер №$1 — $FQDN</h3>
<!--h3 style="background-color:LightGray;">Heading 3</h3-->
<p>
<!-- Описание, инструкции -->
</p>
<table>
    <!--caption>Данные источника статистики</caption-->
    <tr>
        <td>Server name</td>
        <td>$(hostname -f)</td>
    </tr>
    <tr>
        <td>IP address</td>
        <td>$IP</td>
    </tr>
    <tr>
        <td>proftpd service status</td>
        <td$(red2 "$proftpd_status")>$proftpd_status</td>
    </tr>
    <tr>
        <td>/data, /var usage</td>
        <td style="font-family: monospace; white-space: pre">$(df -h /data /var)</td>
    </tr>
    <tr>
        <td>Server uptime</td>
        <td>$(uptime -p)</td>
    </tr>
</table>

<p>
</p>
<table>
    <!--caption>Информация по обновлению cdr файлов и директорий</caption-->
    <tr>
    <th>ФО</th>
    <th>Директория</th>
    <th>Обновление</th>
    <th>ΔT</th>
    <th><i>caches</i> >24h</th>
    <th>δ 24h (Σ cdrs)</th>
    </tr>
EOS
district_rus=(ЦФО ДФО ПФО СЗФО "СКФО + ЮФО" УФО)
IFS=$' '; d=0; for district in $(echo MRG{CE,DV,PV,SZ,UG,UR}); do
    IFS=$'\n'; c=0; for line in $(grep $district $CSVFINAL); do
        if [ $c -eq 0 ]; then
cat >> $HTMLFILE << EOS
    <tr>
    <td rowspan="10">${district_rus[$d]}</td>
	<td$(red1)>$(csv_field 2 $line)</td>
	<td$(red1)>$(csv_field 3 $line)</td>
	<td$(red1)>$(csv_field 5 $line)</td>
	<td$(red1)>$(csv_field 6 $line)</td>
    <td$(red1)>+$(csv_field 9 $line) ($(csv_field 8 $line))</td>
    </tr>
EOS
        else
cat >> $HTMLFILE << EOS
    <tr>
	<td$(red1)>$(csv_field 2 $line)</td>
	<td$(red1)>$(csv_field 3 $line)</td>
	<td$(red1)>$(csv_field 5 $line)</td>
	<td$(red1)>$(csv_field 6 $line)</td>
    <td$(red1)>+$(csv_field 9 $line) ($(csv_field 8 $line))</td>
    </tr>
EOS
        fi;
    ((c++))
    done
((d++))
done

echo -e "</table>\n<hr>" >> $HTMLFILE
echo done.

## Удаление зависших в кешес файлов
# в майл отчете статистике за день отобразится, но.. уже этим же скриптом файлы из кешес будут удалены
# если зачем-то опять будет нужен разбор по зависшим файлам, то делай по итоговому вложению из письма с 6-ого сервера
sudo find /data/MRG[^T]*/*/caches -type f -mmin +1440 -delete

exit 0
