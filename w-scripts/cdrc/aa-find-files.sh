### Constans ###
CSVIN='/home/sa0000sorm3yarus/aa/fl-all-2023-01-26.csv'
CSVOUT='/home/sa0000sorm3yarus/aa/fl-all-2023-01-26.out.csv'
CSVTEMP='/tmp/fl-all-2023-01-26.out.csv'

### Main script ###
touch $CSVOUT
truncate -s 0 $CSVOUT
IFS=$'\n'; for line in $(cat $CSVIN); do
    filename=$(echo "$line" | awk -F"[,/]" '{print $7}')
    find /data/MRG[^T]*/*/2023/01/26 -name "$filename" -printf "$line,outserver$1,%p,%TF %TH:%TM,%s\n" >> $CSVOUT
    echo -n .
done
echo -e '\n'
sha256sum $(cut -d, -f7 $CSVOUT) | cut -d" " -f1 > '/tmp/sha256.txt'
cat $CSVOUT
paste -d, $CSVOUT /tmp/sha256.txt > $CSVTEMP
wc -l $CSVOUT
# rm -f /tmp/sha256.txt
echo done.
