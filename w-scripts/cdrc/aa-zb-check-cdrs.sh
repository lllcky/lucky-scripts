#!/usr/bin/env bash

# $1 - MRGXX/YYYY

### Constants ###
MINS=15
TODAY=$(date +%Y/%m/%d)
YESTERDAY=$(date +%Y/%m/%d -d yesterday)

### Main program ###
subpath=$(echo $1 | sed 's/all/\*/g')
cdr_t=$(find $(echo /data/"$subpath"/$TODAY) -type f -not -mmin +$MINS | wc -l)
cdr_y=$(find $(echo /data/"$subpath"/$YESTERDAY) -type f -not -mmin +$MINS | wc -l)
echo "$((cdr_t + cdr_y))"
