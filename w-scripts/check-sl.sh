#!/usr/bin/bash

# Скрипт для проверки логов sktdm-input (скинера), подсчёта оптик, по которым
# счётчик пакетов равен 0. А также вывод проблемных линков по контейнерам vc-12

TOTAL_STMP=$(awk '/total_count/{print $3}' /opt/sktdm/config/sktdm-input.cfg)

f_empty_stmp() {
    echo "stm с PKT=0"
    grep -hR OKS_HDLC /opt/sktdm/log/stmp_* | gawk 'BEGIN {FS="[ (]"}; /PKT=0,NoISUP=0/{print $3, $5}' | sort -uV
}

# tvv
for ((stm_id = 0; stm_id < $TOTAL_STMP; stm_id += 1)); do
	name_log=""
	name_log=$(ls -1 /opt/sktdm/log/stmp_"${stm_id}"/ | tail -1)
	echo " --- список sl [stm."${stm_id}"] "
	cat /opt/sktdm/log/stmp_"${stm_id}"/"${name_log}" | grep OKS_LINK | awk '{ print $3,$5,$6,$7,$8 }'
	echo;echo " --- проблемные sl "
	[[ $(cat /opt/sktdm/log/stmp_"${stm_id}"/"${name_log}" | grep OKS_HDLC\# | awk '{ print $3,$5,$6 }' | grep -E "PKT=0,NoISUP=0," | wc -l) = 0 ]] &&\
	  echo "[stmp_"${stm_id}"] нет проблемных sl" ||\
	  cat /opt/sktdm/log/stmp_"${stm_id}"/"${name_log}" | grep OKS_HDLC\# | awk '{ print $3,$5,$6 }' | grep -E "PKT=0,NoISUP=0," | sort -V | uniq
	echo " ------------------------------------ "
	name_log=""
done

echo -e "\n\n --- проблемные stmp "
grep -hR OKS_HDLC /opt/sktdm/log/stmp_* | gawk 'BEGIN {FS="[ (]"}; /PKT=0,NoISUP=0/{print $3, $5}' | sort -uV

