#!/usr/bin/bash

# vim: ts=4:sts=4:sw=4:expandtab

ansible-playbook pl_ch_mdstat.yaml
files=$(find ~/md_report -type f | sort)
cat $files >  ~/delme.txt
# Греп файлов вывода mdstat всех серверов по двум регуляркам, получение краткого имени серевера и сортировка
error_servers=$(grep -e "\[10/[0-9]\]" -e "(F)" $files | sed -E 's/.+(ch-[[:digit:]]{3}).+/\1/' | sort -u)
if [ -n "$error_servers" ]; then
    echo -e "
Список серверов с проблемным RAID
=================================
$error_servers
    " | tee -a ~/delme.txt
else
    echo -e "Сервера с проблемным RAID не обнаружены\n" | tee -a ~/delme.txt
fi

echo -e "
Проблемные сервера проверяются по двум параметрам:
    1 Количество дисков не равно 10, т. е. в /proc/mdstat есть паттерны: [10/9], [10/8], ...
    2 Наличие у диска флага \"(F)\"

Просмотр полного  вывода:
\033[01;32mless ~/delme.txt\033[00m
"

# Обрезание лишней информации: всё до "algorithm 2 " влкючительно:
# 46883110912 blocks super 1.2 level 6, 1024k chunk, algorithm 2 [10/10] [UUUUUUUUUU]
# Остаток:
# [10/10] [UUUUUUUUUU]
sed -i '/algorithm/s/.\+algorithm 2 //' ~/delme.txt
