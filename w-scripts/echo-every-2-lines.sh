#!/usr/bin/bash

# Для хорошей работы отступов в Vim:
# vim:ts=4:sw=4:sts=4:et

# Скрипт работает только, где нет пробелов, табуляций в строках!

# Array a из 10 элементов
a=(
string0
string1
string2
string3
string4
string5
string6
string7
string8
string9
)

i=0
# Цикл, пока счётчик i меньше кол-ва элементов массива - и вывод i-го и
# следующего элемента массива
while [ $i -lt ${#a[@]} ]; do
    echo "${a[$i]} ${a[$i + 1]}"
    ((i = i + 2))
done

# Пример выполнения  скрипта /tmp/delme.sh
# [~]$ /tmp/delme.sh
# string0 string1
# string2 string3
# string4 string5
# string6 string7
