#! /usr/bin/bash

INFILE="/opt/sktdm/config/sktdm-input.cfg"
OUTFILE="/home/$(whoami)/aa/iftop-dc.txt"
# Выделяются числовые значения recieve_port из конфига с учётом возможных пробелов (\s)
PORTS=$(gawk '/recieve/{s = gensub(/.+\s*=\s*([[:digit:]]+)\s*$/, "\\1", "g"); print s}' $INFILE)
NOW=$(date +'%F %T %Z')


i=0; for p in $PORTS
do
    if [ $i == 0 ]; then
        filter="port "$p
    else
        filter=$filter" or port "$p
    fi
    ((i++))
done

# echo "filter: -f \"$filter\""
echo -e "\n# Трафик от DC на $(hostname -f)\n\n$NOW\n"
# echo -e "Трафик во второй колонке - средний за 10 с\n"
echo -e "\n## Трафик с сортировкой по скорости\n"
sudo iftop -i bond.150 -f "$filter" -L 1000 -PN -s10 -t | grep '<=\|Total receive' | tee $OUTFILE
echo -e "\n\n## Трафик с сортировкой по портам\n"

for p in $PORTS
do
    p_report=$(grep $p $OUTFILE)
    if [ -n "$p_report" ]; then
        echo "$p$p_report"
    else
        echo -e "$p\tнет трафика"
    fi
done

echo
echo "Receive портов в конфиге: $i"
echo "Receive портов в репорте: $(grep '<=' $OUTFILE | wc -l)"
echo "$(gawk '/Total receive/ {print $1, $2, $3, $5}' $OUTFILE | tee -a $OUTFILE)"
echo
