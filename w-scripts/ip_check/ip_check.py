#!/usr/bin/env python3
"""
== Требование: необходим установленный python3
== Использование скрипта: в терминале запусть исполнение скрипта с двумяаргументами:
./ip_check.py ip.txt net.txt
где первый файл содержит список IP адресов (10.10.10.127), второй файл - номера подсетей с маской (10.86.101.0/24). Имена файлов - произольные. В каждом файле одна строка - одна подсеть или ip.

== Примеры файлов:
$ cat ip.txt
10.68.3.1
10.86.101.255
10.10.10.10
10.10.10.127
10.10.10.128

$ cat net.txt
10.86.101.0/24
10.10.10.0/25
192.168.0.0/16

== Фичи:
- в конце записи в скобках выводится порядковый номер найденного в подсети адреса
- IP и подсети в файлах могут повторяться, это обрабатывается 

== Проверить:
- работу скрипта на винде
- проверить на реальных данных
- проверить работу, когда подсеть имеет вид не номер подсети, а IP с маской
"""

import sys
import ipaddress as ip

file_i = sys.argv[1]
file_n = sys.argv[2]
# set_i = set_n = {}

with open(file_i, encoding="utf-8") as f_i:
    set_ip = set(f_i.read().splitlines())

with open(file_n, encoding="utf-8") as f_n:
    set_net = set(f_n.read().splitlines())

# print("set of ip addrs", sorted(set_ip))
# print("set of networks", sorted(set_net))
# print("type:", type(set_ip), type(set_net))

for n in set_net:
    count = 0
    for i in set_ip:
        if ip.ip_address(i) in ip.ip_network(n):
            count += 1
            print(f"Подсеть {n} содержит IP {i} ({count})")
    if count == 0:
        print(f"Подсеть {n} НЕ содержит IP из файла {file_i} ({count})")
    print()
