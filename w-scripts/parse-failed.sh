#! /usr/bin/bash

err_map=(
	"Estimated query execution time"
    "Read timed out"
    "Connection refused"
    "Table is in readonly mode"
    "File does not exist"
    "Connection loss"
    "Запрос был отменен по таймауту"
)
sum=0  # Для арифметических действий требуется задание начального значения
task_count=$(grep -E -e "^[[:digit:]]{8}" "$1" | wc -l)
echo "-----------------------------------"

for e in "${err_map[@]}"; do
	count=$(grep -E -e "^[[:digit:]]{8}" "$1" | grep -E -e "$e" | wc -l)
	echo "$e: $count"
	sum=$(($sum + $count))
done
echo "-----------------------------------
Всего строк с task_id: $task_count
Строк с ошибками из списка: $sum
Строк остальных: $(($task_count - $sum))
"
