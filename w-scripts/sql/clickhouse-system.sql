select * from system.asynchronous_metric_log limit 20

select * from system.metric_log limit 20

select * from system.query_log limit 20


select * from system.query_thread_log limit 20

select * from system.trace_log limit 20

-- CLI
show create table system.asynchronous_metric_log
SHOW CREATE TABLE system.metric_log
SHOW CREATE TABLE system.query_log
SHOW CREATE TABLE system.query_thread_log
SHOW CREATE TABLE system.trace_log

SELECT
    database,
    table,
    formatReadableSize(sum(data_compressed_bytes) AS size) AS compressed,
    formatReadableSize(sum(data_uncompressed_bytes) AS usize) AS uncompressed,
    round(usize / size, 2) AS compr_rate,
    sum(rows) AS rows,
    count() AS part_count
FROM system.parts
WHERE (active = 1) AND (database = 'system') AND (table LIKE '%')
GROUP BY
    database,
    table
ORDER BY size DESC

# Таблицы с zookeeper_session_expired и в readonly_mode
SELECT toStartOfHour(event_time) as tm, countIf(exception like '%ZooKeeper session has been expired%') as ZooKeeper_session_expired,countIf(exception like '%readonly mode%') as Tables_in_readonly_mode, 1 FROM system.query_log WHERE event_time > '2024-12-01 00:00:00' group by tm order by tm desc

select count(distinct(partition)) from system.parts where database='default' and table='mobile_connections_v2_results'
