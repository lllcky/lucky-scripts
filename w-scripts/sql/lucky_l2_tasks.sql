= Lucky Postgres scripts
Alexei Akulin

select * from pg_stat_activity;

== Запросы по конкретным id

-- инфо по номерам запросов
select start_date, removed_date, execution_start_date, stop_date, date_trunc('second', execution_start_date - start_date) as queue_dur, date_trunc('second', stop_date - execution_start_date) as exec_dur, date_trunc('second', stop_date - start_date) as total_dur, user_name, id, status, queue_type, description, sla_type, sla_period, query_sql, query_params, status_list
from tasks t 
-- where id in (25491157, 25491222, 25491317, 25491451)
where id in (33795686, 33795687, 33795688, 33795689, 33795690, 33795691, 33795692, 33795695, 33795696, 33795697)
limit 100

-- инфо по номерам запросов для скриншотов
-- select start_date, execution_start_date, stop_date, date_trunc('second', execution_start_date - start_date) as queue_dur, date_trunc('second', stop_date - execution_start_date) as exec_dur, date_trunc('second', stop_date - start_date) as total_dur, user_name, id, status, queue_type, sla_type, sla_period, query_sql, query_params, description
select start_date, date_trunc('second', execution_start_date - start_date) as queue_time, date_trunc('second', stop_date - execution_start_date) as exec_time, date_trunc('second', stop_date - start_date) as total_time, user_name, id, status, queue_type, description, sla_type, sla_period, query_sql, query_params
from tasks t 
where id in (33795686, 33795687, 33795688, 33795689, 33795690, 33795691, 33795692, 33795695, 33795696, 33795697)
limit 100



== Запросы по датам и времени

-- инфо по времени
select start_date, execution_start_date, stop_date, date_trunc('second', execution_start_date - start_date) as queue_dur, date_trunc('second', stop_date - execution_start_date) as exec_dur, date_trunc('second', stop_date - start_date) as total_dur, user_name, id, status, queue_type, description, sla_type, sla_period, query_sql, query_params
from tasks t 
where start_date::date = '2023-05-31'
limit 10

-- количество в очереди с началом на дату
select count(start_date)
from tasks t 
where start_date::date = '2023-05-31'
-- and user_name = 'yar_m5'
and status = 'QUEUED'

-- количество запросов на дату
select count(start_date)
from tasks t 
where start_date::date = '2023-08-25'
limit 10


-- количество запросов за последнее время
select count(start_date)
from tasks t
where start_date > now() - interval '5 minutes'
limit 10

-- запросы за последнее время
select start_date, execution_start_date, stop_date, user_name, id,  queue_type, sla_type, sla_period, status, description, query_sql, query_params 
from tasks t
where start_date > now() - interval '5 minutes'
limit 200

-- запросы с ошибками на дату
select start_date, removed_date, execution_start_date, stop_date, date_trunc('second', execution_start_date - start_date) as queue_dur, date_trunc('second', stop_date - execution_start_date) as exec_dur, date_trunc('second', stop_date - start_date) as total_dur, user_name, id, status, queue_type, description, sla_type, sla_period, query_sql, query_params, status_list
from tasks t 
where start_date::date = '2024-05-03'
and queue_type = 'HIVE_SLOW'
and status = 'FAILED'
limit 10



== Запросы по пользователям

-- запросы по кокретным пользователям, поступившие на текущую дату
select start_date, execution_start_date, stop_date, date_trunc('second', execution_start_date - start_date) as queue_dur, date_trunc('second', stop_date - execution_start_date) as exec_dur, date_trunc('second', stop_date - start_date) as total_dur, user_name, id, status, queue_type, description, sla_type, sla_period, query_sql, query_params
from tasks t 
where start_date::date = now()::date 
and user_name in ('pu_mts')
limit 100

-- запросы с группировкой со статусом
select queue_type, user_name, status, count(id) cnt
from sorm_task.tasks
where start_date >= '2023-05-31' AND start_date < '2023-06-01'
group by queue_type, user_name, status

-- запросы с группировкой без статуса
select queue_type, user_name, count(id) cnt
from sorm_task.tasks
where start_date >= '2023-05-31' AND start_date < '2023-06-01'
group by queue_type, user_name

-- запросы с группировкой по управленияем без статуса и типа запросов
select user_name, count(id) cnt
from sorm_task.tasks
where start_date >= '2023-05-31' AND start_date < '2023-06-01'
group by user_name


==  Черновики

select now()::time 

select date_trunc('minute', now()::timestamp(0))





== От коллег

=== Виталиий

SELECT 
    id, status, 
	sla_period, description, user_name, rows_num, queue_type,
	(stop_date - execution_start_date) duration_execut, 
	(execution_start_date - start_date) duration_queue,
	start_date, stop_date, (stop_date - start_date) duration_all
FROM sorm_task.tasks 
WHERE id between 25053215 AND 25053344
AND queue_type = 'HIVE_SLOW'
ORDER BY id DESC;


=== Евгений

/*
 c:\DB
 backup C:\Users\asakulin\AppData\Roaming\DBeaverData\workspace6\General\Scripts
 */

select start_date, removed_date, execution_start_date, stop_date, date_trunc('second', execution_start_date - start_date) as queue_dur, date_trunc('second', stop_date - execution_start_date) as exec_dur, date_trunc('second', stop_date - start_date) as total_dur, user_name, id, status, queue_type, description, sla_type, sla_period, query_sql, query_params, status_list
-- select count(status_list)
from tasks
where status_list::text ~
--'Read timed out'
--'Connection refused'
--'Table is in readonly mode'
--'Connection loss'
'Запрос был отменен по таймауту'
--'Execution Error, return code 1 from org.apache.hadoop.hive.ql.exec.tez.TezTask'
--'Invalid buffer'
--'Failing the application'
--'File does not exist'
--'StackOverflowError'
AND start_date >= '2024-04-01' AND start_date < '2024-04-06'
and status = 'FAILED'

select count(status_list)
from tasks
where status_list::text ~
--'Read timed out'
--'Connection refused'
--'Table is in readonly mode'
--'Connection loss'
--'Запрос был отменен по таймауту'
--'Execution Error, return code 1 from org.apache.hadoop.hive.ql.exec.tez.TezTask'
'Invalid buffer'
--'Failing the application'
--'File does not exist'
--'StackOverflowError'
AND start_date >= '2024-04-01' AND start_date < '2024-04-06'
and status = 'FAILED'



select * from sorm_task.tasks where status = 'FAILED' AND start_date >= '2024-06-25' AND start_date < '2024-06-26' order by id desc

select count(id) from sorm_task.tasks where status = 'FAILED' AND start_date >= '2024-06-25' AND start_date < '2024-06-26'

select count(id) from sorm_task.tasks where status = 'FAILED' AND start_date >= current_date-2 AND start_date < current_date-1

select current_date - 234