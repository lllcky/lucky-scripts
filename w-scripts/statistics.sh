#!/usr/bin/bash
err_map=("Estimated query execution time" "Read timed out" "Connection refused" "Table is in readonly mode" "File does not exist" "Connection loss" "Запрос был отменен по таймауту"
        "Illegal type id 0" "StackOverflowError" "Failing the application" "While parsing a protocol message, the input ended unexpectedly in the middle of a field"
       "End of file reached before reading fully"  "Protocol message contained an invalid tag" "Protocol message tag had invalid wire type" "Operation timeout" "Protocol message end-group tag did not match expected tag"
        "ORC split generation failed with exception"  "Невозможно сгененрировать плоский JSON" "Protocol message end group tag did not match expected tag" "input ended unexpectedly in the middle of a field" 
        "FileFormatException: Malformed ORC file" )

#user="STS_MSO_Group_TAP"
#user ="avstegan"
truncate -s 0 /tmp/stat.txt

file=$(echo failed_$(date -d '-2 days' +%F).csv)
sum=0
count_ch=0
count_hs=0
count_hf=0
e=0

task_count=$(grep -E -e "^[[:digit:]]{8}" "$file" | wc -l)
file=$(echo failed_$(date -d '-2 days' +%F).csv);
echo $file
 echo "-----------------------------------"
# Read the input file line by line using a for loop

for e in "${err_map[@]}"; do

    #numErr=$(grep -E -e "^[[:digit:]]{8}" "$file" | grep -E -e "$e" | wc -l )
    count_ch=$(grep -E -e "^[[:digit:]]{8}" "$file" | grep -E -e "$e" | grep 'CH' | wc -l)
    count_hs=$(grep -E -e "^[[:digit:]]{8}" "$file" | grep -E -e "$e" | grep 'HIVE_SLOW'| wc -l)
    count_hf=$(grep -E -e "^[[:digit:]]{8}" "$file" | grep -E -e "$e" | grep 'HIVE_FAST' | wc -l)

  #if [ "$count_ch" -ne 0 ]; then
  #|| [ "$count_hs" -gt 0 ] || [ "$count_hf" -gt 0 ]; then
    #echo " ошибки CH- $count_ch $e: 'CH' $count_ch" >>/tmp/stat.txt 
  #if [ "$count_hs" -ne 0 ]; then
    #echo " ошибки HS- $count_hs $e: 'HS' $count_hs" >>/tmp/stat.txt
  #if [ "$count_ch" -ne 0 ]; then
    #echo " ошибки HS- $count_hf $e: 'HF' $count_hf" >>/tmp/stat.txt

  if [[ $count_ch -ne 0 || $count_hs -ne 0 || $count_hf -ne 0 ]]; then
             echo "$e - CH: $count_ch, HS: $count_hs, HF: $count_hf" | sed 's/,\? \?[[:alpha:]]\{2\}: 0,\?//g'  >>/tmp/stat.txt
                     sum=$((sum + count_ch + count_hs + count_hf))

#    sum=$(($sum + $count_ch + $count_hs + $count_hf)) ;

  fi

done
echo "-----------------------------------
Всего строк с task_id:$task_count
Строк с ошибками из списка: $sum
Строк остальных: $(($task_count - $sum))
Вывод ошибок:
CH-ClickHouse
HS-Hive_SLOW
HF-Hive_FAST " >> /tmp/stat.txt

 cat /tmp/stat.txt | mailx -s "Statistics_HIVE_CH $(date -d '-2days' +'%F %T')" -a /tmp/stat.txt avstegan@mts.ru


#ошибки CH- $count_ch $e: 'CH' $count_ch | sed 's/,\? \?[[:alpha:]]\{2\}: 0,\?//g'
#ошибки CH- $count_hs $e: 'HS' $count_hs | sed 's/,\? \?[[:alpha:]]\{2\}: 0,\?//g'
#ошибки CH- $count_hf $e: 'HF' $count_hf | sed 's/,\? \?[[:alpha:]]\{2\}: 0,\?//g'


#echo "Statistics_HIVE_CH\n " |  mailx -s "Statistics $(date -d '-2days' +'%F %T')" -a /tmp/stat.txt avstegan@mts.ru

# < /tmp/stat.txt

#sed 's/,\? \?[[:alpha:]]\{2\}: 0,\?//g'

