# tunce_generator


## Требования к дополнительному модулю

Для выполнянения скрипта требуется локальная установка **python**. Также для функционирования скрипта требуется модуль `openpyxl`. Его можно установить через `pip` (ниже показано с предварительным обновлением в Windows 10).

```
PS > python.exe -m pip install --upgrade pip
Requirement already satisfied: pip in c:\p\python\python310\lib\site-packages (22.2.2)
Collecting pip
  Downloading pip-23.3.2-py3-none-any.whl (2.1 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 2.1/2.1 MB 6.4 MB/s eta 0:00:00
Installing collected packages: pip
  Attempting uninstall: pip
    Found existing installation: pip 22.2.2
    Uninstalling pip-22.2.2:
      Successfully uninstalled pip-22.2.2
Successfully installed pip-23.3.2

PS > pip install openpyxl
Collecting openpyxl
  Downloading openpyxl-3.1.2-py2.py3-none-any.whl (249 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 250.0/250.0 kB 1.9 MB/s eta 0:00:00
Collecting et-xmlfile (from openpyxl)
  Downloading et_xmlfile-1.1.0-py3-none-any.whl (4.7 kB)
Installing collected packages: et-xmlfile, openpyxl
Successfully installed et-xmlfile-1.1.0 openpyxl-3.1.2
```


## Скрипт для генерации tunce.xml

Пример использования: в Linux **./gen.py -f <path_to_file>** или в Windows **python gen.py -f <path_to_file>**

Допустимо вместо ключа **-f** использовать ключ **--file**

Результатом работы скрипта является при входных данных в виде **.xlsx** файла является файл **tunce.xml**, в котором во всех элементах **signal_link** и **station_pair** без атрибута **id**.

Результатом работы скрипта является при входных данных в виде **.xml** файла является файл **tunce.xml**, в котором во всех элементах **signal_link** и **station_pair** в атрибут **id** проставлен порядковый номер элемента. Xml документ должен иметь корневой тег **\<tunce\>**


## Функционал на 21.12.2023

Проверки:
- корректность значений stm_id
- соответствие NI префиксов
- корректность числа указанных контейнеров VC12 на stm
- корректность разности start_cic и end_cic
- полное наличие или полное отсутствие параметров
- идентичность protection_group в рамках группы opc-dpc-protection_group


## Ссылки на исходные данные

Исходные данные в формате xlsx находятся на сетевом ресурсе:

```text
none
```

