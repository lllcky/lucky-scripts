#!/usr/bin/bash

# OUT="/home/$(whoami)/tunes.xml"
# OUT="/mnt/c/w/git-tunes-gen"
OUT="./tunes.xml"

cat "$@" > $OUT
sed -i '/<\/\?tunes>/d; /^\s*$/d' $OUT

sed -i '
1i <tunes>
$a </tunes>
' $OUT

# cat $OUT
