#!/usr/bin/env python3

import os.path
import sys
import xml.etree.ElementTree as ET

# Переменные
pcm_list = []
st_pair_list = []

# Константы
LINE = "-" * 64
TUNES = sys.argv[1]
# TUNES = "tunes/tunes-20231114-novosib-mob-tvv-pstn-sl_03_pcmplus1.xml"

print(LINE)
print("Filename:", os.path.abspath(TUNES), sep='')
print(LINE)
if os.path.exists(TUNES) == False:
    raise Exception('Указанный файл не существует')

tree = ET.parse(TUNES)
root = tree.getroot()
for i in root.iter('station_pair'):
    st_pair = {i.attrib['opc'], i.attrib['dpc']}
    if st_pair in st_pair_list:
        print(f'[Duplicated]::пара станций st_pair {st_pair}')
    else:
        st_pair_list.append(st_pair)
# print('len(st_pair_list) =', len(st_pair_list))

for i in root.iter('pcm'):
    pcm_stm_vc = [i.attrib['stm_pair'], i.attrib['vc12_num']]
    if pcm_stm_vc in pcm_list:
        print(f'[Duplicated]::канал {pcm_stm_vc} (stm_pair, vc12_num)')
    else:
        pcm_list.append(pcm_stm_vc)
# print('len(pcm_list) =', len(pcm_list))

for station_pair in root.iter('station_pair'):
    # print(station_pair.tag, station_pair.attrib)
    pcm_id_list = []
    for pcm in station_pair:
        # print('   ', pcm.tag, pcm.attrib)
        # print(pcm.attrib['id'])
        if pcm.attrib['id'] in pcm_id_list:
            print(f"[Duplicated]::для пары станций с id {station_pair.attrib['id']} ({station_pair.attrib['opc']}/{station_pair.attrib['dpc']}) дублируется pcm c id {pcm.attrib['id']}")
        else:
            pcm_id_list.append(pcm.attrib['id'])

with open(TUNES, 'r', encoding='utf-8') as f:
    lines = f.readlines()
    for l in enumerate(lines):
        if '""' in l[1]:
            print("[Empty_value]:line_", l[0], ':', l[1].strip(), sep='')

