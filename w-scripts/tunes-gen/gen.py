from openpyxl import Workbook
from openpyxl import load_workbook
import argparse
import time
from datetime import datetime
import xml.etree.ElementTree as xt

import logging
import math
import os

# Нумерация строк ?
# Нумерация столбцов с 0
# Нумерация листов с 0

COLUMN_COUNT = 32
STM_ID_F = 4
VC12_F = 16
OPC_F = 21
DPC_F = 22
TS_SS7_F = 23
ST_CIC_F = 25
END_CIC_F = 26
REACH_METHOD_F = 27
PROTEC_GROUP_F = 28
DATE_CHANGE = 32

"""
# Индекс столбцов указан с 1
STM_ID_F = 4
VC12_F = 17
OPC_F = 22
DPC_F = 23
TS_SS7_F = 24
ST_CIC_F = 26
END_CIC_F = 27
REACH_METHOD_F = 28
PROTEC_GROUP_F = 29
DATE_CHANGE = 33
"""

REACH_METHODS_DICT = {
    "LS": "LS (mtp2)",
    "SRF": "STP (SRF)",
    "SoL": "SRForLS",
    "PRI": "PRI"
}

CORRECT_HEADERS = [
    (STM_ID_F, "stm_id на STM-C".lower()),
    (VC12_F, "VC12 (E1 в STM-1)".lower()),
    (OPC_F, "OwnPC (dec)".lower()),
    (DPC_F, "DPC (dec)".lower()),
    (TS_SS7_F, "TS SS7".lower()),
    (ST_CIC_F, "Start CIC".lower()),
    (END_CIC_F, "End CIC".lower()),
    (REACH_METHOD_F, "каким образом достигается сигнальная точка".lower()),
    #(PROTEC_GROUP_F, "протекшен групп, осн\рез".lower()),  # расширить
    #(DATE_CHANGE, "Дата изменения".lower())
]

# logging.root.setLevel(logging.DEBUG)
log_name = 'tunes_gen_' + datetime.now().strftime("%Y_%m_%d__%H_%M_%S") + '.log'
logging.basicConfig(filename = log_name, encoding='utf-8', level=logging.INFO)
xml_root = xt.ElementTree()

def check_line_3(sheet):
    row3 = sheet[2]  # Нумерация с 0
    non_val = False
    for i in range(1,14):
        if row3[i].value == None:
            non_val = True
    if non_val:
        return 4
    return 3

# Проверяем заголовки на соответствие
def check_headers (sheet, start_row_num):
    row = sheet[1:1]
    errors = []
    for hdr in CORRECT_HEADERS:
        row_val = row[(hdr[0] - 1)].value.lower().replace('\n', '')
        if row_val == None:
            errors.append("In document column name "  + str(hdr[0] - 1) + " is empty")
            continue
        if row_val != hdr[1] :
            errors.append("Failed column names check expected "+ hdr[1]
                           + " on " + str(hdr[0]) + ", document contains: " + row[hdr[0]-1].value)
    if len(errors) != 0:
        for e in errors:
            logging.error(e)
        return False
    return True

# Проверка stm_id на пустые значения
def check_column_stm_id(sheet, start_row, stop_row):
    stm_ids = sheet[start_row:stop_row]
    is_errorneous = False

    error_string = "STM_ID has empty value in rows: "
    nan_string = "STM_ID is not a number in rows: "
    error_row_num = []
    nan_row_num = []
    #TODO добавить возможность проверки отображаемого значения а не фактического
    for i in enumerate(stm_ids):
        if i[1][STM_ID_F - 1].value == None:
            is_errorneous = True
            error_row_num.append(str(i[0] + start_row))
        else:
            try:
                n = int(i[1][STM_ID_F - 1].internal_value)
            except ValueError:
                nan_row_num.append(str(i[0] + start_row))
                is_errorneous = True

    if is_errorneous:
        if len(error_row_num) != 0:
            logging.error(error_string + ", ".join(error_row_num))
        if len(nan_row_num) != 0:
            logging.error(nan_string + ", ".join(nan_row_num))
        return False

    return True

# Проверка совпадения network interface
def check_opc_dpc_ni(sheet, start_row, stop_row):
    errors = []
    rows = sheet[start_row:stop_row]
    row_counter = start_row
    for r in enumerate(rows, start_row):
        if r[1][OPC_F - 1].value == None or r[1][DPC_F - 1].value == None:
            continue

        opc_str = str(r[1][OPC_F - 1].value).strip()
        dpc_str = str(r[1][OPC_F - 1].value).strip()

        if str.isdigit(opc_str[0]) == False or str.isdigit(dpc_str[0]) == False:
            errors.append(f'Row {r[0]}: Incorrect NI\'s, OPC: {opc_str}, DPC: {dpc_str}')
            continue

        if len(opc_str) < 3 or len(dpc_str) < 3:
            errors.append(f'Row {r[0]}: Too short NI\'s, OPC: {opc_str}, DPC: {dpc_str}')
            continue
        
        if opc_str[0] != dpc_str[0]:
            errors.append(f'Row {r[0]}: Not matched NI\'s , OPC: {opc_str}, DPC: {dpc_str}')

    if len(errors) != 0:
        for e in errors:
            logging.error(e)
        return False
    return True

def check_vc12_count(sheet, start_row, stop_row):
    errors = []
    rows = sheet[start_row:stop_row]
    row_counter = start_row
    error_flag = False
    stm_id_dict = {}
    for r in rows:
        if r[STM_ID_F - 1].value in stm_id_dict.keys():
            # Если значение пустое
            if r[VC12_F - 1].value == None:
                logging.error("Container value is empty; row " + str(row_counter))
                error_flag = True
                row_counter += 1
                continue
            # Проверка на дубликат
            if r[VC12_F - 1].value in stm_id_dict[r[STM_ID_F - 1].value]:
                logging.error("Duplicate container value; container: " + str(r[VC12_F - 1].value) +
                        "stm_id: " + str(stm_id_dict[r[STM_ID_F - 1].value]) + "row: " + str(row_counter))
                error_flag = True
                row_counter += 1
                continue
            # Проверка на размер(что не больше 63 контейнеров)
            if len(stm_id_dict[r[STM_ID_F - 1].value]) > 63:
                logging.error("Containers count more than 63; stm_id: " + str(stm_id_dict[r[STM_ID_F - 1].value])
                 + "container: " + str(r[VC12_F - 1].value) + "row: " + str(row_counter))
                error_flag = True
                row_counter += 1
                continue

            lst = stm_id_dict[r[STM_ID_F - 1].value]
            lst.append(r[VC12_F - 1].value)
            stm_id_dict[r[STM_ID_F - 1].value] = lst
        else:
            stm_id_dict[r[STM_ID_F - 1].value] = [r[VC12_F - 1].value]
        row_counter += 1

    logging.debug(len(stm_id_dict))
    return not error_flag

def check_cic_diff(sheet, start_row, stop_row):
    rows = sheet[start_row:stop_row]
    # row_counter = start_row
    error_flag = False
    for r in enumerate(rows):
        if r[1][ST_CIC_F - 1].value == None and r[1][END_CIC_F - 1].value == None:
            continue
        try:
            st_cic = int(str(r[1][ST_CIC_F - 1].value))
            end_cic = int(str(r[1][END_CIC_F - 1].value))
        except ValueError:
            logging.error("Start CIC or end CIC not a number; row: " + str(r[0]+start_row))
            error_flag = True
            continue
        diff = end_cic - st_cic
        if diff > 30:
            logging.error("Too big difference between CIC's; diff: " + str(diff) + ", row: "+ str(r[0]+start_row))
            error_flag = True
            continue
        e1_st = math.floor((st_cic - 1) / 32) + 1
        e1_end = math.floor((end_cic - 1) / 32) + 1
        if e1_st != e1_end:
            logging.error("Row: " + str(r[0] + start_row) + ", Start CIC and End CIC in different E1: start " + str(e1_st) + ", end: " + str(e1_end))
            error_flag = True
    return not error_flag

def check_tuple_of_six_params(sheet, start_row, stop_row):
    fields_description = {
        0: "OPC",
        1: "DPC",
        2: "Start CIC",
        3: "End CIC",
        4: "Signal point reach method",
#       5: "Protection group"
    }
    rows = sheet[start_row:stop_row]
    errors = []
    for r in enumerate(rows):
        # По заполненным файлам оптимальна проверка полей 0-4
        t5 = (r[1][OPC_F - 1].value, r[1][DPC_F - 1].value, r[1][ST_CIC_F - 1].value, \
              r[1][END_CIC_F - 1].value, r[1][REACH_METHOD_F -1].value)#, r[1][PROTEC_GROUP_F - 1].value)
        # Либо должны пристутствовать все требуемые поля, либо они все должны отсутствовать иначе ошибка
        if (t5[0] == None and t5[1] == None and t5[2] == None and t5[3] == None and t5[4] == None) or \
            (t5[0] != None and t5[1] != None and t5[2] != None and t5[3] != None and t5[4] != None):
            continue
        error_string = "Row " + str(r[0] + start_row) + ": Missing fields: "

        mf = []
        for f in enumerate(t5):
            mf.append(f'{fields_description[f[0]]} = "{str(f[1])}"')
        errors.append(error_string + ", ".join(mf))

    if len(errors) == 0:
        return True
    else:
        for e in errors:
            logging.error(e)
        return False

def group_by_opc_dpc_pg(sheet, start_row, end_row):
    groups = {}
    rows = sheet[start_row:end_row]
    for r in enumerate(rows):
        if (r[1][OPC_F - 1].value == None or r[1][DPC_F - 1].value == None):
            continue
        key = (str(r[1][OPC_F - 1].value).strip(), str(r[1][DPC_F - 1].value).strip(), str(r[1][PROTEC_GROUP_F - 1].value).strip())
        if key not in groups.keys():
            groups[key] = []
        rv = []
        for v in r[1]:
            rv += [v.value,]
        try:
            if rv[ST_CIC_F - 1] != None:
                rv[ST_CIC_F - 1] = int(rv[ST_CIC_F - 1])
            if rv[END_CIC_F - 1] != None:
                rv[END_CIC_F - 1] = int(rv[END_CIC_F - 1])
        except ValueError:
            logging.error(f'Row:  + {str(r[0]+start_row)} Cannot conver to number Start CIC  = "{rv[ST_CIC_F - 1]}" or End CIC  "{rv[END_CIC_F - 1]}" to number; ')
        groups[key].append(tuple(rv))

    for g in groups:
        logging.debug(g)
    logging.debug("Groups count: " + str(len(groups)))
    return groups


# <!-- ============================================================================================== -->
# <!-- комментарий: есть прямой SL (2 шт.) + достигается ч\з SRF ? -->
# <!-- комментарий: SRF prio=1, SL prio=2, SL прописан всего 1 (по распечатке MSSBC1, m3rsp 20230913 -->
# <!-- ats: "MSS1NSK", opc: "2-8860", dpc: "2-8388", trunks: "MEGA2SO", mse: "MSE1-22-1", amc: "AMC3-1-2" -->

def generate_comment_text(row):
    element_list = []

    element_list.append(xt.Comment('=============================================================================================='))
    n = f'ats: {row[OPC_F - 3]}, opc: {row[OPC_F - 1]}, dpc: {row[OPC_F - 1]}, {row[STM_ID_F + 4]},  trunks: {row[OPC_F - 2]}, mse: {row[PROTEC_GROUP_F]}, \
          amc{row[STM_ID_F + 4] + row[VC12_F - 2]} '
    element_list.append(xt.Comment(n))

    return True

def generate_xml_structure(groups_dict, file_desc):
    tunes = xt.Element("tunes")

    for g in groups_dict:
        signal_links = []
        pcms = []
        st_pair_element = xt.Element("station_pair", {"opc": str(g[0][2:]), "dpc": str(g[1][2:]), "sltype":"0"})
        reach_method = None
        if g[0] != 'None' and g[1] != 'None':
            groups_dict[g].sort(key= lambda a : a[ST_CIC_F - 1])
        for r in groups_dict[g]:
            # Ищем сигнальные линки
            have_signal_link = False
            rmv = r[REACH_METHOD_F - 1]
            if rmv != None:

                if rmv == REACH_METHODS_DICT["PRI"]:
                    logging.warning("PRI signal point reach methods")
                    continue

                if reach_method == None:
                        reach_method = rmv
                elif reach_method != rmv:
                    logging.error(f'Mismatch signal point reach methods. Previous: {reach_method}, current: {rmv}')
                    continue

                if rmv == REACH_METHODS_DICT["LS"] and r[TS_SS7_F -1] != None:
                    signal_links.append((str(r[STM_ID_F - 1]).strip(), str(r[VC12_F - 1]).strip(), int(r[TS_SS7_F - 1])))
                    have_signal_link = True
                if rmv == REACH_METHODS_DICT["SoL"] and r[TS_SS7_F -1] != None:
                    signal_links.append((str(r[STM_ID_F - 1]).strip(), str(r[VC12_F - 1]).strip(), int(r[TS_SS7_F - 1])))
                    have_signal_link = True

            if r[ST_CIC_F - 1] == None or r[ST_CIC_F - 1] == None:
                continue

            pcm_id = math.floor((r[ST_CIC_F - 1] - 1) / 32) # + 1
            start_ts = ((r[ST_CIC_F - 1] - 1) % 32) + 1
            end_ts = ((r[END_CIC_F - 1] - 1) % 32) + 1

            ts_num = ""
            lst = list(range(start_ts, end_ts + 1))
            if have_signal_link:
                try:
                    lst.remove(signal_links[-1][2])
                except ValueError:
                    logging.info("Trying to delete from ts_num value " + str(signal_links[-1][2]) \
                                 + ", ts_start " + str(start_ts))
            ts_num = ",".join([str(x) for x in lst])
            pcms.append(xt.Element("pcm", {"id": str(pcm_id), "stm_pair": str(r[STM_ID_F - 1]), "vc12_num": str(r[VC12_F - 1]), "ts_num": ts_num}))

        for l in signal_links:
            sl = xt.Element("signal_link", {"stm_pair": l[0], "vc12_num": l[1], "ts_num": str(l[2])})
            if g[2] == 'основной' or g[2] == 'осн' or g[2] == "None":
                tunes.append(sl)
            else:
                tunes.append(xt.Comment(xt.tostring(sl, encoding="unicode")))

        st_pair_element.attrib["sltype"] = encode_reach_method(REACH_METHODS_DICT, reach_method)
        for p in pcms:
            st_pair_element.append(p)

        if g[2] == 'основной' or g[2] == 'осн' or g[2] == "None":
            tunes.append(st_pair_element)
        else:
            tunes.append(xt.Comment(xt.tostring(st_pair_element, encoding="unicode")))
    xt.indent(tunes, space = "    ", level = 0)
    xt.ElementTree(tunes).write(file_desc, encoding="unicode")
    return True

def encode_reach_method(dict, method):
    if method == dict["LS"]:
        return "0"
    elif method == dict["SRF"]:
        return "1"
    elif method == dict["SoL"]:
        return "0"
    return "0"

def enumerate_xml(file):
    tree = xt.parse(file)
    root = tree.getroot()
    signal_link_id = 0
    station_pair_id = 0
    if root.tag != "tunes":
        logging.error(f'Xml {file} does not start with <tunes>')
        return False

    for t in root:
        if t.tag == "signal_link":
            t.set('id', str(signal_link_id))
            signal_link_id += 1
        elif t.tag == "station_pair":
            t.set('id', str(station_pair_id))
            station_pair_id += 1
    with open(file, 'w') as f:
        tree.write(f, encoding="unicode")

    return True

#int main(int argc, char* argv[])
print("Started............................")
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-f', '--file', action='store',
                    help='Name of file to be processed')

args = parser.parse_args()
start_time = time.time()

file_name, file_ext = os.path.splitext(args.file)
logging.info(f'Filename: {args.file}')

if file_ext == ".xml":
    enumerate_xml(args.file)
    os._exit(0)

workbook = load_workbook(args.file, data_only=True, read_only=True)
sheet = workbook["описание потоков"]

# Доступ к листу по индексу
#nsheet = workbook._sheets[1]  # Нумерация с 0

if sheet.max_column < COLUMN_COUNT:
    logging.error("Too few columns")
    os._exit(1)

start_row = check_line_3(sheet)
stop_row =  sheet.max_row
error = False

# Проверки корректности значений
if not check_headers(sheet, start_row):
    os._exit(3)

if not check_column_stm_id(sheet, start_row, stop_row):
    error = True
if not check_opc_dpc_ni(sheet, start_row, stop_row):
    error = True
if not check_vc12_count(sheet, start_row, stop_row):
    error = True
if not check_cic_diff(sheet, start_row, stop_row):
    error = True
if not check_tuple_of_six_params(sheet, start_row, stop_row):
    error = True

if error == True:
   os._exit(4)

# Группировка по OPC-DPC-Protection
group = group_by_opc_dpc_pg(sheet, start_row, stop_row)

with open("tunes.xml", "w") as d:
    generate_xml_structure(group, d)

print("--- Elapsed time: %s seconds ---" % (time.time() - start_time))
logging.debug(sheet.max_row)
logging.debug(sheet.max_row)
