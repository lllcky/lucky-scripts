#!/usr/bin/python

import os
import re
import xml.etree.ElementTree as ET

# Переменные
found = False         # флаг найденной пары stm/vc-12 (для отсева дубликатов)
pc_eth = []           # поинткоды sigtran
pc_hdlc = []          # поинткоды tdm
pcm = []              # лист pcm
pcm_line = []         # лист pcm
pcm_set = set()       # сет голосовых каналов (для отсева дубликатов, когда одному stm/vc-12 принадлежит не один набор CIC
sl_eth = []           # лист sigtran линков
sl_hdlc = []          # лист tdm линков
sl_set = set()        # сет линков (для отсева дубликатов)
st_eth_key = set()    # идентификаторы пар станций tdm
st_hdlc_key = set()   # идентификаторы пар станций sigtran
st_key = set()        # уникальные идентификаторы пар станций
pcm_id = sl_id = st_id = 0
tree = ET.fromstring("<tunes></tunes>")

# Константы
LINE = "-" * 30
LOGDIR = 'log-2023-11-07'
LOGDIR = 'log-2023-11-22'
LOGDIR = 'log-2023-11-23'
LOGDIR = 'log03-2023-11-23'
LOGDIR = 'log03-2023-11-24'
LOGDIR = 'log04-2023-11-24'
LOGDIR = 'log05-2023-11-24'
LOGDIR = 'log05-2023-11-24_2'
LOGDIR = 'log03-2023-11-24_3'
LOGDIR = 'log04-2023-11-27'
LOGDIR = 'log02-2023-11-29'
TS0 = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31'
TS1 = '2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31'
TS16 = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31'

# Функция f_st_key определяет уникальный идентификатор k пары станций
# Функции f_opc(), f_dpc(), обратные f_st_key (находят opc, dpc по k)
# Функция f_findfiles() выводит рекурсивный список всех файлов в директории path по регулярному выражению regex
def f_st_key(a, b):
    return (min(a, b) << 16) + max(a, b)

def f_dpc(k):
    return k >> 16

def f_opc(k):
    return k - (k >> 16 << 16)

def f_findfiles(path, regex):
    re_obj = re.compile(regex)
    res = []
    for root, dirs, fnames in os.walk(path):
        for fname in fnames:
            if re_obj.match(fname):
                res.append(os.path.join(root, fname))
    return res


main_files = f_findfiles(LOGDIR, r'main_')
print("### length of main_files:", len(main_files))


# == Обработка main log (st_hdlc_key)
# Формирование листа с opc, dpc из файла
"""
for l in lines:
    l = re.sub('.*OPC=(\d+).+DPC=(\d+).+PCM=(\d+).*', r'\1 \2', l)
    # l = re.sub('.*OPC=(\d+).+DPC=(\d+).+PCM=(\d+).*', r'\1 \2 \3', l)
    l = l.split()
    # print(l)
    pc_hdlc.append(l)
print()
"""

# Конкатенация всех файлов main в один лист
with open('out_main.txt', 'w') as outfile:
    for f in main_files:
        with open(f) as infile:
            for l in infile:
                if "HDLC STATION DETECT" in l:
                    l = re.sub('.*OPC=(\d+).+DPC=(\d+).+PCM=(\d+).*', r'\1 \2 HDLC', l)
                    outfile.write(l)
                    l = l.split()
                    pc_hdlc.append(l)
                elif "ETH STATION DETECT" in l:
                    l = re.sub('.*OPC=(\d+).+DPC=(\d+).+PCM=(\d+).*', r'\1 \2 ETH', l)
                    outfile.write(l)
                    l = l.split()
                    pc_eth.append(l)

# Формирование листа с уникальными ключами пар станций
for i in pc_hdlc:
    k = f_st_key(int(i[0]), int(i[1]))
    st_hdlc_key.add(k)

st_hdlc_key = list(st_hdlc_key)
pc_hdlc = []
for i in st_hdlc_key:
    pc_hdlc.append([f_opc(i), f_dpc(i)])
pc_hdlc.sort()

print("pc_hdlc:")
[print(i) for i in pc_hdlc]
print()
print("pc_eth:")
[print(i) for i in pc_eth]
print()

for i in pc_eth:
    k = f_st_key(int(i[0]), int(i[1]))
    # Если пара есть в tdm, не добавлять её в sigtran
    if k not in st_hdlc_key:
        st_eth_key.add(k)

st_eth_key = list(st_eth_key)
pc_eth = []
for i in st_eth_key:
    pc_eth.append([f_opc(i), f_dpc(i)])
pc_eth.sort()
[print(i) for i in pc_eth]

# print(pc_hdlc)
print("1" + LINE)
# print("st_eth_key:", st_eth_key)
print('### length of st_hdlc_key:', len(st_hdlc_key))
print('### length of st_eth_key:', len(st_eth_key))

# Проверка и вывод дублированных пар станций
for i in st_hdlc_key:
    # print(f_opc(i), f_dpc(i), i, '')
    st_key.add(i)
for i in st_eth_key:
    if i in st_key:
        print("Повторная пара - есть и в tdm, и в sigtran:", f_opc(i), f_dpc(i))
    st_key.add(i)
print()
print('### Общее количество уникальных station_pair:')
print("### st_key:", len(st_key))
print("2" + LINE)




# == Обработка st_ логов (pcm)
st_files = f_findfiles(LOGDIR, r'st_')
print("### lenght of st_files:", len(st_files))

#  Конкатенация всех файлов st в один лист
with open('out_st.txt', 'w') as outfile:
    for f in st_files:
        with open(f) as infile:
            for l in infile:
                # Условие 'ts = 1' исключает ложные линки с ts = 0
                if 'Voice Detect' in l:
                    # обрезание даты, времени
                    l = l[26:]
                    pcm_line.append(l)
                    outfile.write(l)
print('### length of pcm_line:', len(pcm_line))

# Парсинг инфо по pcm: Stp, PCM, FRWD, BKWD в сет
for i in pcm_line:
    # i = re.sub('.+Stp\((\d+)\).+PCM=(\d+).+FRWD=\d+\((\d+)/(\d+)\).+BKWD=\d+\((\d+)/(\d+)\)', r'\1 \2 \3 \4 \5 \6', i)
    i = re.sub('\[st_(\d+)_(\d+).+Stp\((\d+)\).+PCM=(\d+).+FRWD=\d+\((\d+)/(\d+)\).+BKWD=\d+\((\d+)/(\d+)\)', r'\3 \1 \2 \4 \5 \6 \7 \8', i)
    pcm_set.add(i.strip())

# Конвертация сета в лист с разбивкой по значениям
pcm_line = []
for i in pcm_set:
    pcm_line.append(i.split())

# Фильтрация листа pcm_list, остаются только строки где поля 4=6 и 5=7, отфильтрованный лист - pcm
# pcm_line {0: Stp, 1: opc, 2: dpc, 3: pcm, 4: frwd_stm_pair, 5: frwd_vc12, 6: bkwd_stm_pair, 7: bkwd_vc12}
pcm_set = set()
for i in pcm_line:
    if i[4] == i[6] and i[5] == i[7]:
        # поля 4 = 6, а 5 = 7, поэтому 6, 7 отбрасываются
        i = i[:6]
        pcm.append(i)
        # Отдельный pcm_set для отбора уникальных пар stm_pair/vc12
        j = str(i[4] + ' ' + i[5])
        pcm_set.add(j)


# Натуральная сортировка: по stm_pair, pcm, vc12 - для наглядного формирования xml
pcm.sort(key=lambda x: (int(x[0]), int(x[3]), int(x[4])))




# == Обработка stm_proc логов (sl_hdlc)
stm_proc_files = f_findfiles(LOGDIR, r'stm_proc')

#  Конкатенация всех файлов stm_proc в один, остаются только уникальные строки
with open('out_sl.txt', 'w', encoding='utf8') as outfile:
    for f in stm_proc_files:
        with open(f) as infile:
            for l in infile:
                # Условие 'ts = 1' исключает ложные линки с ts = 0, т. е. подходят линки и с ts = 1 и 16
                if 'HDLC_DETECT' and 'ts = 1' in l:
                    # обрезание даты, времени и хвоста
                    l = l[26:]
                    l = re.sub('; pkt =.+', '', l)
                    sl_set.add(l)
                    outfile.write(l)


# Парсинг инфо по сигнальным линкам stmp, e1, ts
for i in sl_set:
    i = re.sub('.*stmp_(\d+).+e1 = (\d+).+ts = (\d+).*', r'\1 \2 \3', i)
    sl_hdlc.append(i.strip().split())

# Натуральная сортировка: по stm_pair, по e1
# sl_hdlc {0: stm_pair, 1: e1 (vc-12), 2: ts}
sl_hdlc.sort(key=lambda i: (int(i[0]), int(i[1])))




# == Техвывод
print("### len(pcm):", len(pcm), "как предыдущий pcm_set с условием frwd = bkwd")
print("### len(pcm_set):", len(pcm_set), "(уникальных stm_pair + vc12)")
print("### pcm_set:", pcm_set)
print("\npcm {0: Stp, 1: opc, 2: dpc, 3: pcm, 4: stm_pair, 5: vc12}")
[print(i) for i in pcm]
print('\n### length of sl_set:', len(sl_set))
print("sl_hdlc {0: stm_pair, 1: e1 (vc-12), 2: ts}")
[print(i) for i in sl_hdlc]



# == Формирование дерева xml
for i in sl_hdlc:
    ET.SubElement(tree, 'signal_link', {'id': str(sl_id), 'stm_pair': str(i[0]), 'vc12_num': str(i[1]), 'ts_num': str(i[2])})
    sl_id += 1
for i in pc_hdlc:
    ET.SubElement(tree, 'station_pair', attrib={'id': str(st_id), 'opc': str(i[0]), 'dpc': str(i[1]), 'sltype': '0'})
    st_id += 1
for i in pc_eth:
    ET.SubElement(tree, 'station_pair', attrib={'id': str(st_id), 'opc': str(i[0]), 'dpc': str(i[1]), 'sltype': '1'})
    st_id += 1

#st_pairs_id = set()
#for i in pcm:
#    if i[0] not in st_pairs_id:
#        if [int(i[1]), int(i[2])] in pc_hdlc:
#            ET.SubElement(tree, 'station_pair', attrib={'id': i[0], 'opc': str(i[1]), 'dpc': str(i[2]), 'sltype': '0'})
#            st_pairs_id.add(i[0])
#        elif [int(i[1]), int(i[2])] in pc_eth:
#            ET.SubElement(tree, 'station_pair', attrib={'id': i[0], 'opc': str(i[1]), 'dpc': str(i[2]), 'sltype': '1'})
#            st_pairs_id.add(i[0])
#        else:
#            raise Exception(f'[ERROR] Пара станций Stp({i[0]}): {i[1]}, {i[2]} не найдена ни в pc_hdlc, ни в pc_eth, но имеется в pcm')

print("3" + LINE)
# ET.SubElement(tree[1], 'pcm id="34"')
# ET.SubElement(tree[1], 'pcm id="35"')
# print(ET.iselement(tree))
# tree[1][0].text='"text for tree3[1]"'

# Добавление pcm в соответсвующие station_pair + сравнение со списком линков sl_hdlc и добавление атрибута ts_num
# pcm - 0: Stp, 1: pcm, 2: stm_pair, 3: vc12
# sl_hdlc - 0: stm_pair, 1: e1, 2: ts
for el in tree.iter('station_pair'):
    # print(el.attrib['id'])
    for i in pcm:
        found = False    # флаг найденного pcm с линком
        # Раскомментировать для вырезки дубликатов пар станций. Принцип построен на удалении из pcm_set пар станций, которые уже были добавлены в дерево. Если уникальной пары станций уже нет, то цикл пропускается
        if el.attrib['id'] == str(i[0]):
        # if el.attrib['id'] == str(i[0]) and str(i[4] + ' ' + i[5]) in pcm_set:
            for j in sl_hdlc:
                if i[4] == j[0] and i[5] == j[1]:
                    if j[2] == '1':
                        # Элемент для отладки - меняет название элемента для наглядности
                        # ET.SubElement(el, 'pcm1', {'id': i[3], 'stm_pair': i[4], 'vc12_num': i[5], 'ts_num': TS1})
                        ET.SubElement(el, 'pcm', {'id': i[3], 'stm_pair': i[4], 'vc12_num': i[5], 'ts_num': TS1})
                        found = True
                    elif j[2] == '16':
                        # Элемент для отладки - меняет название элемента для наглядности
                        # ET.SubElement(el, 'pcm16', {'id': i[3], 'stm_pair': i[4], 'vc12_num': i[5], 'ts_num': TS16})
                        ET.SubElement(el, 'pcm', {'id': i[3], 'stm_pair': i[4], 'vc12_num': i[5], 'ts_num': TS16})
                        found = True
            if found == False:
                # Элемент для отладки - меняет название элемента для наглядности
                # ET.SubElement(el, 'pcm0', {'id': i[3], 'stm_pair': i[4], 'vc12_num': i[5], 'ts_num': TS0})
                ET.SubElement(el, 'pcm', {'id': i[3], 'stm_pair': i[4], 'vc12_num': i[5], 'ts_num': TS0})
            pcm_set.discard(str(i[4] + ' ' + i[5]))


# == Подготовка и запись xml файла
ET.indent(tree, space='    ')
doc = ET.ElementTree(tree)
doc.write("out.xml", encoding="unicode", xml_declaration=False)
