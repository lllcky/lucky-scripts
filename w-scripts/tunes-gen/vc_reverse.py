#!/usr/bin/env python3

"""
Подсказки по использованию скрипта в CLI:

$ python vc_reverse.py -h
> python vc_reverse.py -h

"""

import argparse
import re

# Парсинг аргументов командной строки
parser = argparse.ArgumentParser(prog="vc_reverse.py",
                                 description="Cкрипт для конвертации схем контейнеров vc-12 в файлах конфигурации tunes.xml. \
                                              Вывод организуется в файл out_stmp.xml, если файл не указан в опции -o",
                                 epilog="example: python vc_reverse.py -f tunes.xml 0, 32, 62",
                                 # Для мультистрочных описаний - не используется:
                                 # formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument("-f", "--file", action="store", help="входной xml-файл", required=True)
parser.add_argument("-o", "--out", action="store", help="выходной xml-файл (опционально)", required=False)
parser.add_argument("integers", metavar="N", type=int, nargs="+", help="значения stm_id через пробел для конвертации")
args = parser.parse_args()
# print("args.file:", args.file)
# print("args.integers:", args.integers)
# print(type(args.out))

INFILE = args.file

if args.out == None:
    OUTFILE = "out_stmp.xml"
else:
    OUTFILE = args.out

# Карта реверсной нумерации для vc-12 в stm-1
VC12_MAP = (1, 22, 43, 4, 25, 46, 7, 28, 49, 10,
            31, 52, 13, 34, 55, 16, 37, 58, 19, 40,
            61, 2, 23, 44, 5, 26, 47, 8, 29, 50,
            11, 32, 53, 14, 35, 56, 17, 38, 59, 20,
            41, 62, 3, 24, 45, 6, 27, 48, 9, 30,
            51, 12, 33, 54, 15, 36, 57, 18, 39, 60,
            21, 42, 63)

infile = open(INFILE, 'r', encoding='utf8')
outfile = open(OUTFILE, 'w', encoding='utf8', newline='\n')

print("----------------------------")
print("Входной файл:", INFILE)
print("Выходной файл:", OUTFILE)
print("----------------------------")

for line_num, l in enumerate(infile):
    # Поиск всех stm_pair из агрументов командной строки для каждой строки
    for stmp in args.integers:
        # Для строк, где нашлись vc12_num="xx" и указанная stm_pair, производится замена e1 на e1r (reverse)
        if 'vc12_num' and f'stm_pair="{stmp}"' in l:
            m = re.search('vc12_num="(\\d+)"', l)
            e1 = int(m.group(1))
            e1r = VC12_MAP[e1 - 1]
            # Не перезаписывает строки с одинаковым vc12 - нет смысла
            if e1 != e1r:
                print(f'строка {line_num + 1}: stmp-{stmp}: {e1} \u2192 {e1r}')
                l = re.sub(f'vc12_num="{e1}"', f'vc12_num="{e1r}"', l)
    # Запись строк с отрезанием справа добаленных пробелов или табуляций
    # outfile.write(l.rstrip())
    outfile.write(l)

infile.close()
outfile.close()

