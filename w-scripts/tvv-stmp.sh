#!/usr/bin/bash

for stm_id in {0..4}; do
	name_log=""
	name_log=$(ls -1 /opt/sktdm/log/stmp_"${stm_id}"/ | tail -1)
	echo " --- список sl [stm."${stm_id}"] "
	cat /opt/sktdm/log/stmp_"${stm_id}"/"${name_log}" |grep OKS_LINK |awk '{ print $3,$5,$6,$7,$8 }'
	echo;echo " --- проблемные sl "
	[[ $(cat /opt/sktdm/log/stmp_"${stm_id}"/"${name_log}" |grep OKS_HDLC# |awk '{ print $3,$5,$6 }' |grep -E "PKT=0,NoISUP=0," |wc -l) = 0 ]] &&\
	  echo "[stmp_"${stm_id}"] нет проблемных sl" ||\
	  cat /opt/sktdm/log/stmp_"${stm_id}"/"${name_log}" |grep OKS_HDLC# |awk '{ print $3,$5,$6 }' |grep -E "PKT=0,NoISUP=0," |sort -V |uniq
	echo " ------------------------------------ "
	name_log=""
done
